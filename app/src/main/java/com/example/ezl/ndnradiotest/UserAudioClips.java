package com.example.ezl.ndnradiotest;

public class UserAudioClips {

    UserAudioClips(UserBufferedAudioManager syncAudioClips, UserBufferedAudioManager voiceAndRetrievalFailureAudioClips) {
        this.syncAudioClips = syncAudioClips;
        this.voiceAndRetrievalFailureAudioClips = voiceAndRetrievalFailureAudioClips;
    }

    public UserBufferedAudioManager syncAudioClips;
    public UserBufferedAudioManager voiceAndRetrievalFailureAudioClips;

}
