package com.example.ezl.ndnradiotest;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Random;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import static com.example.ezl.ndnradiotest.LongByteArrayConversionHelpers.LONG_NUM_BYTES;
import static com.example.ezl.ndnradiotest.LongByteArrayConversionHelpers.bytesToLong;
import static com.example.ezl.ndnradiotest.LongByteArrayConversionHelpers.longToBytes;
import static com.example.ezl.ndnradiotest.LongByteArrayConversionHelpers.longToTime;
import static com.example.ezl.ndnradiotest.MainActivity.HEARTBEAT_LIFETIME;
import static com.example.ezl.ndnradiotest.MainActivity.MAX_HEARTBEAT_RANDOMIZER;

public class NDNRadioService extends ChronoSyncService {

    private static final String TAG = "NDNRadioService";

    public static final int
            USER_ID = 0,
            SEQ_NUM = 1;

    public static final String
            ACTION_RECEIVED = "ACTION_RECEIVED",
            ACTION_STOP = "ACTION_STOP",
            ACTION_SEND = "ACTION_SEND",
            ACTION_GOT_AUDIO_BYTES = "ACTION_GOT_AUDIO_BYTES",
            ACTION_GOT_SYNC = "ACTION_GOT_SYNC",
            ACTION_STARTED_FETCH = "ACTION_STARTED_FETCH",
            ACTION_FINISHED_FETCH = "ACTION_FINISHED_FETCH",
            ACTION_FAILED_FETCH = "ACTION_FAILED_FETCH",
            ACTION_NFD_FAILURE_NOTIFICATION = "ACTION_NFD_FAILURE_NOTIFICATION",
            ACTION_NFD_RECOVERY_NOTIFICATION = "ACTION_NFD_RECOVERY_NOTIFICATION",
            ACTION_WIFI_FAILURE_NOTIFICATION = "ACTION_WIFI_FAILURE_NOTIFICATION",
            ACTION_GOT_NAME_FOR_USERID = "ACTION_GOT_NAME_FOR_USERID",
            ACTION_PUBLISH = "ACTION_PUBLISH",
            ACTION_NFDC_MESSAGE = "ACTION_NFDC_MESSAGE";

    public static final String
            EXTRA_CHANNEL = "EXTRA_CHANNEL",
            EXTRA_NAME = "EXTRA_NAME",
            EXTRA_AUDIO_INFO = "EXTRA_AUDIO_INFO",
            EXTRA_SYNC_INFO = "EXTRA_SYNC_INFO",
            EXTRA_AUDIO_BYTES = "EXTRA_AUDIO_BYTES",
            EXTRA_RADIO_INFO = "EXTRA_RADIO_INFO",
            EXTRA_USER_ID_NAME_INFO = "EXTRA_USER_ID_NAME_INFO",
            EXTRA_LOGINFO = "EXTRA_LOGINFO",
            EXTRA_SEQNUM = "EXTRA_SEQNUM",
            EXTRA_NFDC_MESSAGE = "EXTRA_NFDC_MESSAGE";

    public static final int
            CHANNEL = 0,
            NAME = 1,
            SEGMENT_INTEREST_MAX_REATTEMPTS = 2,
            SEGMENT_INTEREST_LIFETIME = 3,
            AP_IP_ADDRESS = 4;

    public static final int
            NAME_FOR_ID_NOTIF_USER_ID = 0,
            NAME_FOR_ID_NOTIF_NAME = 1;


    private String m_channel;
    private String m_name;
    private int m_segment_interest_lifetime;
    private int m_segment_interest_max_reattempts;
    private String m_data_prefix;
    private String m_broadcast_prefix;
    private String m_ap_ip_address;
    public String m_uuid;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (intent != null) {
            String action = intent.getAction();
            Log.d(TAG, "received intent " + action);
        }

        return START_STICKY;
    }

    @Override
    public void onDestroy() {

        super.onDestroy();
    }

    @Override
    protected void handleApplicationData(byte[] receivedData, String userId, long seqNum) {

        Intent receivedAudioBytesIntent = new Intent(NDNRadioService.ACTION_GOT_AUDIO_BYTES);

        receivedAudioBytesIntent.putExtra(NDNRadioService.EXTRA_AUDIO_BYTES, receivedData);
        String[] audioInfoArray = new String[2];
        audioInfoArray[USER_ID] = userId;
        audioInfoArray[SEQ_NUM] = Long.toString(seqNum);
        receivedAudioBytesIntent.putExtra(NDNRadioService.EXTRA_AUDIO_INFO, audioInfoArray);

        LocalBroadcastManager.getInstance(this).sendBroadcast(receivedAudioBytesIntent);


        Log.d(TAG, "NDN radio service got data from user id " + userId + " with seq num " + seqNum);
        Log.d(TAG, "Length of received data in ndn radio service: " + receivedData.length);

    }

    @Override
    protected void sendSyncNotification(String userId, long seqNum, String userName) {
        sendIntent(userId, seqNum, userName, NDNRadioService.ACTION_GOT_SYNC);
    }

    @Override
    protected void sendStartedFetchingNotification(String userId, long seqNum, String userName) {
        sendIntent(userId, seqNum, userName, NDNRadioService.ACTION_STARTED_FETCH);
    }

    @Override
    protected void sendFinishedFetchingNotification(String userId, long seqNum, String userName, long numSegments) {
        sendFinishedFetchingIntent(userId, seqNum, userName, NDNRadioService.ACTION_FINISHED_FETCH, numSegments);
    }

    @Override
    protected void sendFailedFetchingNotification(String userId, long seqNum, String userName) {
        sendIntent(userId, seqNum, userName, NDNRadioService.ACTION_FAILED_FETCH);
    }

    @Override
    protected void sendPublishNotification(long seqNum) {
        sendPublishIntent(seqNum);
    }

    private void sendIntent(String userId, long seqNum, String userName, String intentAction) {
        Intent receivedSyncIntent = new Intent(intentAction);

        String[] logInfoArray = new String[4];
        logInfoArray[LOGINFO_USER_ID] = userId;
        logInfoArray[LOGINFO_SEQ_NUM] = Long.toString(seqNum);
        logInfoArray[LOGINFO_USER_NAME] = userName;
        receivedSyncIntent.putExtra(NDNRadioService.EXTRA_LOGINFO, logInfoArray);

        LocalBroadcastManager.getInstance(this).sendBroadcast(receivedSyncIntent);

        Log.d(TAG, "NDN radio service got log notification from user id " + userId + " " +
                "with seq num " + seqNum + " and username " + userName + " and " +
                "intent action " + intentAction);
    }

    private void sendPublishIntent(long seqNum) {
        Intent publishIntent = new Intent(ACTION_PUBLISH);

        publishIntent.putExtra(EXTRA_SEQNUM, seqNum);

        LocalBroadcastManager.getInstance(this).sendBroadcast(publishIntent);

        Log.d(TAG, "NDN radio service got publish notification with seq num " + seqNum);
    }

    private void sendFinishedFetchingIntent(String userId, long seqNum, String userName, String intentAction, long numSegments) {
        Intent receivedSyncIntent = new Intent(intentAction);

        String[] logInfoArray = new String[5];
        logInfoArray[LOGINFO_USER_ID] = userId;
        logInfoArray[LOGINFO_SEQ_NUM] = Long.toString(seqNum);
        logInfoArray[LOGINFO_USER_NAME] = userName;
        logInfoArray[LOGINFO_NUM_SEGMENTS] = Long.toString(numSegments);
        receivedSyncIntent.putExtra(NDNRadioService.EXTRA_LOGINFO, logInfoArray);

        LocalBroadcastManager.getInstance(this).sendBroadcast(receivedSyncIntent);

        Log.d(TAG, "NDN radio service got log notification from user id " + userId + " " +
                "with seq num " + seqNum + " and username " + userName + " and " +
                "intent action " + intentAction + " and num segments " + numSegments);
    }

    @Override
    protected void sendNFDFailureNotification() {

        Log.d(TAG, "Send NFD failure notification got called");

        Intent nfdFailureIntent = new Intent(NDNRadioService.ACTION_NFD_FAILURE_NOTIFICATION);

        LocalBroadcastManager.getInstance(this).sendBroadcast(nfdFailureIntent);
    }

    @Override
    protected void sendNFDRecoveryNotification() {

        Log.d(TAG, "Send NFD Recovery notification got called");

        Intent nfdRecoveryIntent = new Intent(NDNRadioService.ACTION_NFD_RECOVERY_NOTIFICATION);

        LocalBroadcastManager.getInstance(this).sendBroadcast(nfdRecoveryIntent);

    }

    @Override
    protected void sendNFDCRelatedMessage(String message) {
        Intent nfdcRelatedMessage = new Intent(ACTION_NFDC_MESSAGE);
        nfdcRelatedMessage.putExtra(EXTRA_NFDC_MESSAGE, message);
        LocalBroadcastManager.getInstance(this).sendBroadcast(nfdcRelatedMessage);
    }

    @Override
    protected void sendWifiFailureNotification() {
        Intent wifiFailureIntent = new Intent(NDNRadioService.ACTION_WIFI_FAILURE_NOTIFICATION);

        LocalBroadcastManager.getInstance(this).sendBroadcast(wifiFailureIntent);
    }

    @Override
    protected void sendNameForUserIdNotification(String userId, String name) {

        String[] info = new String[2];
        info[NAME_FOR_ID_NOTIF_USER_ID] = userId;
        info[NAME_FOR_ID_NOTIF_NAME] = name;

        Intent nameForUserIdIntent = new Intent(ACTION_GOT_NAME_FOR_USERID);
        nameForUserIdIntent.putExtra(EXTRA_USER_ID_NAME_INFO, info);

        LocalBroadcastManager.getInstance(this).sendBroadcast(nameForUserIdIntent);

    }

    @Override
    protected void doApplicationSetup() {

    }

    protected void sendMessage(ArrayList<byte[]> audioClipArrayList) {
        Log.d(TAG, "Radio service got a call to send message, here are the bytes:");
        //String str = "";
        //for (int i = 0; i < data.length; i++) {
        //    str += data[i] + " ";
        //}
        //Log.d(TAG, str);
        send(audioClipArrayList);
    }

    private final IBinder mBinder = new NDNRadioService.LocalBinder();

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "NDN radio service got bound.");

        String uuid = UUID.randomUUID().toString();

        m_uuid = uuid;
        String[] radioInfo = intent.getStringArrayExtra(NDNRadioService.EXTRA_RADIO_INFO);
        m_channel = radioInfo[NDNRadioService.CHANNEL];
        m_name = radioInfo[NDNRadioService.NAME];
        m_segment_interest_lifetime = Integer.parseInt(radioInfo[NDNRadioService.SEGMENT_INTEREST_LIFETIME]);
        m_segment_interest_max_reattempts = Integer.parseInt(radioInfo[NDNRadioService.SEGMENT_INTEREST_MAX_REATTEMPTS]);
        m_ap_ip_address = radioInfo[NDNRadioService.AP_IP_ADDRESS];
        m_data_prefix = getString(R.string.data_prefix) + "/" + uuid;
        m_broadcast_prefix = getString(R.string.broadcast_prefix) + "/" + m_channel;

        initializeService(m_data_prefix,
                m_broadcast_prefix, m_name, m_segment_interest_max_reattempts, m_segment_interest_lifetime, m_ap_ip_address);

        MainActivity.m_user_prefix_display.setText(m_data_prefix);
        MainActivity.m_broadcast_prefix_display.setText(m_broadcast_prefix);

        return mBinder;
    }

    public class LocalBinder extends Binder {
        NDNRadioService getService() {
            return NDNRadioService.this;
        }
    }

    public static IntentFilter getIntentFilter() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(NDNRadioService.ACTION_GOT_AUDIO_BYTES);
        intentFilter.addAction(NDNRadioService.ACTION_GOT_SYNC);
        intentFilter.addAction(NDNRadioService.ACTION_NFD_FAILURE_NOTIFICATION);
        intentFilter.addAction(NDNRadioService.ACTION_WIFI_FAILURE_NOTIFICATION);
        intentFilter.addAction(NDNRadioService.ACTION_NFD_RECOVERY_NOTIFICATION);
        intentFilter.addAction(NDNRadioService.ACTION_GOT_NAME_FOR_USERID);
        intentFilter.addAction(NDNRadioService.ACTION_STARTED_FETCH);
        intentFilter.addAction(NDNRadioService.ACTION_FINISHED_FETCH);
        intentFilter.addAction(NDNRadioService.ACTION_FAILED_FETCH);
        intentFilter.addAction(NDNRadioService.ACTION_PUBLISH);
        intentFilter.addAction(NDNRadioService.ACTION_NFDC_MESSAGE);

        return intentFilter;
    }
}
