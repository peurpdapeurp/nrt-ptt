package com.example.ezl.ndnradiotest;

import android.support.annotation.NonNull;

public class PrioritizedSyncStateInfo implements Comparable<PrioritizedSyncStateInfo> {

    PrioritizedSyncStateInfo(ChronoSync2013Custom.SyncState syncState, boolean isRecovery) {
        this.syncState = syncState;
        this.isRecovery = isRecovery;
        this.priority = syncState.getSequenceNo();
    }

    ChronoSync2013Custom.SyncState syncState;
    boolean isRecovery;
    long priority;

    @Override
    public int compareTo(@NonNull PrioritizedSyncStateInfo other) {
        if (priority < other.priority)
            return -1;
        else if (priority > other.priority)
            return 1;

        return 0;
    }
}
