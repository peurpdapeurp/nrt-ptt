package com.example.ezl.ndnradiotest;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

//import com.intel.jndn.management.ManagementException;
//import com.intel.jndn.management.Nfdc;

import com.intel.jndn.management.ManagementException;
import com.intel.jndn.management.Nfdc;
import com.intel.jndn.management.types.FaceStatus;

import net.named_data.jndn.ComponentType;
import net.named_data.jndn.Data;
import net.named_data.jndn.Face;
import net.named_data.jndn.Interest;
import net.named_data.jndn.InterestFilter;
import net.named_data.jndn.KeyLocator;
import net.named_data.jndn.Name;
import net.named_data.jndn.NetworkNack;
import net.named_data.jndn.OnData;
import net.named_data.jndn.OnInterestCallback;
import net.named_data.jndn.OnNetworkNack;
import net.named_data.jndn.OnRegisterFailed;
import net.named_data.jndn.OnRegisterSuccess;
import net.named_data.jndn.OnTimeout;
import net.named_data.jndn.encoding.EncodingException;
import net.named_data.jndn.encoding.der.DerDecodingException;
import net.named_data.jndn.security.KeyChain;
import net.named_data.jndn.security.SecurityException;
import net.named_data.jndn.security.VerificationHelpers;
import net.named_data.jndn.security.certificate.IdentityCertificate;
import net.named_data.jndn.security.pib.Pib;
import net.named_data.jndn.security.pib.PibIdentity;
import net.named_data.jndn.security.pib.PibImpl;
import net.named_data.jndn.security.tpm.Tpm;
import net.named_data.jndn.security.tpm.TpmBackEnd;
import net.named_data.jndn.security.v2.CertificateV2;
import net.named_data.jndn.util.Blob;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public abstract class ChronoSyncService extends Service {

    private static final String TAG = "ChronoSyncService";
    private static final double SYNC_LIFETIME = 5000.0;
    private static final boolean RETRIEVE_STALE_MESSAGES_BY_DEFAULT = true;

    public static final String
            SYNC_STATE_FROM_USER_SYNC_STATE_INFO_MANAGER = "SYNC_STATE_FROM_USER_SYNC_STATE_INFO_MANAGER";

    public static final String
            EXTRA_SYNC_STATE_INFO_STRINGS = "EXTRA_SYNC_STATE_INFO_STRINGS",
            EXTRA_SYNC_STATE_INFO_LONGS = "EXTRA_SYNC_STATE_INFO_LONGS",
            EXTRA_SYNC_STATE_INFO_IS_RECOVERY = "EXTRA_SYNC_STATE_INFO_IS_RECOVERY";

    public static final int
            SYNC_STATE_INFO_SYNC_SESSION = 0,
            SYNC_STATE_INFO_SEQ_NUM = 1,
            SYNC_STATE_INFO_DATA_PREFIX = 0,
            SYNC_STATE_INFO_USER_ID = 1;

    boolean firstTimeLaunch = true;

    /* Intent constants */
    public static final String
            INTENT_PREFIX = "edu.ucla.cs.ChronoChat." + TAG + ".",
            BCAST_ERROR = INTENT_PREFIX + "BCAST_ERROR",
            EXTRA_ERROR_CODE = INTENT_PREFIX + "EXTRA_ERROR_CODE";

    private final String
            NETWORK_THREAD_STOPPED = "NETWORK_THREAD_STOPPED";


    public final static String
            ACTION_FAILED_RETRIEVAL = "ACTION_FAILED_RETRIEVAL";

    public final static String
            EXTRA_FAILURE_INFO = "EXTRA_FAILURE_INFO";

    public final static int
            SEQ_NUM = 0,
            USER_ID = 1,
            USER_NAME = 2,
            ERROR_CODE = 3;

    public final static int
            LOGINFO_SEQ_NUM = 0,
            LOGINFO_USER_ID = 1,
            LOGINFO_USER_NAME = 2,
            LOGINFO_NUM_SEGMENTS = 3;

    enum ErrorCode {NFD_PROBLEM, OTHER_EXCEPTION}

    private ErrorCode m_raisedErrorCode = null;

    public static ChronoSyncService chronoSyncService;

    protected Face m_face;
    private Name m_dataPrefix, m_broadcastPrefix;

    private ChronoSync2013Custom sync;
    private volatile boolean m_networkThreadShouldStop,
            m_syncInitialized = false;
    private final boolean m_shouldRetrieveStaleData;
    private Thread currentNetworkThread;
    private Thread currentHeartbeatInterestThread;
    private KeyChain m_keyChain;
    private Map<String, Long> m_nextSeqNumToRequest;
    private MemoryContentCacheCustom m_sentData;
    private MemoryContentCacheCustom m_userCertificateDataPackets;
    private int m_session;
    private int m_currentSequenceNumber = 0;
    private CertificateV2 m_myCert;
    //private IdentityCertificate m_myCert;
    private String m_myName;
    private PibIdentity m_myIdentity;
    //private Name m_myIdentity;
    public ConcurrentHashMap<String, CertificateV2> m_userCertificates;
    public ConcurrentHashMap<String, UserBufferedSyncStateInfosManager> m_userSyncStates;
    private HashMap<String, String> m_userNames;

    private SegmentFetcherCustom m_dataFetcher;
    private int m_maxReattemptsPerSegmentInterest;
    private int m_segmentInterestTimeout;
    private String m_ap_ip_address_with_prefix_and_suffix;

    BroadcastReceiver syncStateInfoListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (action.equals(SYNC_STATE_FROM_USER_SYNC_STATE_INFO_MANAGER)) {

                String[] stringInfo = intent.getStringArrayExtra(EXTRA_SYNC_STATE_INFO_STRINGS);
                long[] longInfo = intent.getLongArrayExtra(EXTRA_SYNC_STATE_INFO_LONGS);

                long syncSession = longInfo[SYNC_STATE_INFO_SYNC_SESSION],
                        syncSeqNum = longInfo[SYNC_STATE_INFO_SEQ_NUM];
                String syncDataPrefix = stringInfo[SYNC_STATE_INFO_DATA_PREFIX],
                        userId = stringInfo[SYNC_STATE_INFO_USER_ID];

                boolean isRecovery = intent.getExtras().getBoolean(EXTRA_SYNC_STATE_INFO_IS_RECOVERY);

                class OneShotTask implements Runnable {
                    long syncSession, syncSeqNum;
                    String syncDataPrefix, userId;
                    boolean isRecovery;

                    OneShotTask(long syncSession, long syncSeqNum, String syncDataPrefix, String userId, boolean isRecovery) {
                        this.syncSession = syncSession;
                        this.syncSeqNum = syncSeqNum;
                        this.syncDataPrefix = syncDataPrefix;
                        this.userId = userId;
                        this.isRecovery = isRecovery;
                    }

                    public void run() {
                        processSyncState(syncSession, syncSeqNum, syncDataPrefix, userId, isRecovery);
                    }
                }
                Thread t = new Thread(new OneShotTask(syncSession, syncSeqNum, syncDataPrefix, userId, isRecovery));
                t.start();

            } else {
                Log.w(TAG, "sync state info listener got unexpected action from intent, " + action);
            }
        }
    };

    BroadcastReceiver networkThreadStoppedListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            Log.d(TAG, "Network thread stopped listener is now calling start network thread");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            startNetworkThread();
        }
    };

    BroadcastReceiver nfdStatusListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals(NDNRadioService.ACTION_NFD_FAILURE_NOTIFICATION)) {
                restartNetworkThread();
            } else if (intent.getAction().equals(NDNRadioService.ACTION_NFD_RECOVERY_NOTIFICATION)) {

            }

        }
    };

    private void restartNetworkThread() {
        Log.d(TAG, "Attempting to restart network thread");

        stopNetworkThread();
    }

    private void createAndStartNewNetworkThread() {
        Thread networkThread = new Thread(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "network thread started");
                int registerCounter = 0;
                try {
                    if (m_face == null)
                        m_face = new Face(getString(R.string.face_uri));
                    setCommandSigningInfo();
                    registerDataPrefix();
                    setUpChronoSync();
                    doApplicationSetup();
                    if (firstTimeLaunch) {
                        firstTimeLaunch = false;
                        sendInitialMessage();
                    } else {
                        if (!m_networkThreadShouldStop)
                            sendNFDRecoveryNotification();
                    }

                } catch (Exception e) {
                    raiseError("error during network thread initialization",
                            ErrorCode.OTHER_EXCEPTION, e);
                }
                while (!m_networkThreadShouldStop) {
                    publishSeqNumsIfNeeded();
                    try {
                        m_face.processEvents();
                        Thread.sleep(100); // avoid hammering the CPU
                    } catch (IOException e) {
                        Log.d(TAG, "Entered the IO Exception place");
                        raiseError("error in processEvents loop", ErrorCode.NFD_PROBLEM, e);
                    } catch (Exception e) {
                        Log.d(TAG, "Entered the generic exception place");
                        raiseError("error in processEvents loop", ErrorCode.OTHER_EXCEPTION, e);

                    }

                    if (registerCounter > 30) {
                        try {
                            boolean foundFace = false;
                            // create the new face
                            for (FaceStatus faceStatus : Nfdc.getFaceList(m_face)) {
                                Log.d(TAG, "Remote uri of this face: " + faceStatus.getRemoteUri());
                                if (faceStatus.getRemoteUri().toString().equals(m_ap_ip_address_with_prefix_and_suffix)) {
                                    Log.d(TAG, "Face was already created, face id: " +
                                            faceStatus.getFaceId());
                                    foundFace = true;

                                    // run base method
                                    try {
                                        Nfdc.register(m_face, faceStatus.getFaceId(), new Name("/"), 0);
                                    }
                                    catch (ManagementException e) {
                                        e.printStackTrace();
                                        sendNFDCRelatedMessage(e.getMessage());
                                    }
                                }
                            }

                            if (!foundFace) {
                                Log.d(TAG, "Didn't find face created already, creating face.");
                                try {
                                    Nfdc.createFace(m_face, m_ap_ip_address_with_prefix_and_suffix);
                                }
                                catch (ManagementException e) {
                                    e.printStackTrace();
                                    sendNFDCRelatedMessage(e.getMessage());
                                }
                            }




                        } catch (ManagementException e) {
                            Log.d(TAG, "MANAGEMENT EXCEPTIONNNN");
                            //raiseError("error registering the / route", ErrorCode.NFD_PROBLEM, e);
                            e.printStackTrace();

                        }
                        registerCounter = 0;
                    }
                    else {
                        registerCounter++;
                    }

                }
                if (m_keyChain == null) {
                    Log.d(TAG, "KEYCHAIN WAS NULL BEFORE FINAL CLEANUP");
                }
                doFinalCleanup();
                handleAnyRaisedError();
                Log.d(TAG, "network thread stopped");
                LocalBroadcastManager.getInstance(ChronoSyncService.this).sendBroadcast(new Intent(NETWORK_THREAD_STOPPED));
            }
        });
        currentNetworkThread = networkThread;
        m_networkThreadShouldStop = false;
        currentNetworkThread.start();
    }

    OnData onData = new OnData() {
        @Override
        public void onData(Interest interest, Data data) {

        }
    };

    OnTimeout onTimeout = new OnTimeout() {
        @Override
        public void onTimeout(Interest interest) {

        }
    };

    OnNetworkNack onNack = new OnNetworkNack() {
        @Override
        public void onNetworkNack(Interest interest, NetworkNack networkNack) {

        }
    };


    public ChronoSyncService() {
        this(RETRIEVE_STALE_MESSAGES_BY_DEFAULT);
    }

    public ChronoSyncService(boolean shouldRetrieveStaleData) {
        this.m_shouldRetrieveStaleData = shouldRetrieveStaleData;
    }


    @Override
    public void onDestroy() {
        // attempt to clean up after ourselves
        Log.d(TAG, "onDestroy() cleaning up...");
        stopNetworkThreadAndBlockUntilDone();
        Log.d(TAG, "service destroyed");
        LocalBroadcastManager.getInstance(this).unregisterReceiver(nfdStatusListener);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(networkThreadStoppedListener);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(syncStateInfoListener);
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        stopSelf();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    protected void initializeService(String dataPrefixStr, String broadcastPrefixStr, String name, int maxReattemptsPerSegmentInterest,
                                     int segmentInterestTimeout, String ap_ip_address) {

        chronoSyncService = this;

        Log.d(TAG, "(re)initializing service...");
        m_face = new Face(getString(R.string.face_uri));
        m_dataPrefix = new Name(dataPrefixStr);
        m_broadcastPrefix = new Name(broadcastPrefixStr);
        m_nextSeqNumToRequest = Collections.synchronizedMap(new HashMap<String, Long>());
        m_userCertificates = new ConcurrentHashMap<>();
        m_userNames = new HashMap<>();
        m_userSyncStates = new ConcurrentHashMap<>();
        m_sentData = new MemoryContentCacheCustom(m_face);
        m_userCertificateDataPackets = new MemoryContentCacheCustom(m_face);
        m_session = (int) (System.currentTimeMillis() / 1000);
        m_myName = name;
        m_segmentInterestTimeout = segmentInterestTimeout;
        m_maxReattemptsPerSegmentInterest = maxReattemptsPerSegmentInterest;
        m_ap_ip_address_with_prefix_and_suffix = getString(R.string.AP_IP_ADDRESS_PREFIX) + ap_ip_address +
                                                    getString(R.string.AP_IP_ADDRESS_SUFFIX);
        LocalBroadcastManager.getInstance(this).registerReceiver(nfdStatusListener, NDNRadioService.getIntentFilter());
        LocalBroadcastManager.getInstance(this).registerReceiver(networkThreadStoppedListener, new IntentFilter(NETWORK_THREAD_STOPPED));
        LocalBroadcastManager.getInstance(this).registerReceiver(syncStateInfoListener, new IntentFilter(SYNC_STATE_FROM_USER_SYNC_STATE_INFO_MANAGER));
        initializeKeyChain();
        startNetworkThread();
        Log.d(TAG, "service initialized");
    }

    private void startNetworkThread() {
        createAndStartNewNetworkThread();
    }

    private void stopNetworkThread() {
        m_networkThreadShouldStop = true;
    }

    protected boolean networkThreadIsRunning() {
        return currentNetworkThread.isAlive();
    }

    private void stopNetworkThreadAndBlockUntilDone() {
        stopNetworkThread();
        Log.d(TAG, "waiting for network thread to stop...");
        while (currentNetworkThread.isAlive()) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                Log.e(TAG, "interruption while waiting for network thread to stop", e);
            }
        }
    }

    private void doFinalCleanup() {
        Log.d(TAG, "cleaning up and resetting service...");
        m_syncInitialized = false;
        if (sync != null) sync.shutdown();
        if (m_face != null) m_face.shutdown();
        m_face = null;
        sync = null;
        Log.d(TAG, "service cleanup/reset complete");
        if (m_keyChain == null) {
            Log.d(TAG, "M_KEYCHAIN WAS NULL AFTER FINAL CLEANUP");
        }
    }

    private void initializeKeyChain() {
        Log.d(TAG, "initializing keychain");
        try {
            m_keyChain = new KeyChain("pib-memory:", "tpm-memory:");
        } catch (KeyChain.Error error) {
            error.printStackTrace();
        } catch (PibImpl.Error error) {
            error.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        m_keyChain.setFace(m_face);
        m_myIdentity = null;
        try {
            m_myIdentity = m_keyChain.createIdentityV2(new Name(m_myName));
            m_myCert = m_myIdentity.getDefaultKey().getDefaultCertificate();
            m_keyChain.setDefaultIdentity(m_myIdentity);
        } catch (Pib.Error error) {
            error.printStackTrace();
        } catch (PibImpl.Error error) {
            error.printStackTrace();
        } catch (Tpm.Error error) {
            error.printStackTrace();
        } catch (TpmBackEnd.Error error) {
            error.printStackTrace();
        } catch (KeyChain.Error error) {
            error.printStackTrace();
        }
    }

    private void setCommandSigningInfo() {
        Log.d(TAG, "setting command signing info");
        Name defaultCertificateName;
        try {
            defaultCertificateName = m_keyChain.getDefaultCertificateName();
        } catch (SecurityException e) {
            Log.e(TAG, "UNABLE TO GET DEFAULT CERTIFICATE NAME BECAUSE OF SECURITY EXCEPTION");
            return;
        }
        m_face.setCommandSigningInfo(m_keyChain, defaultCertificateName);
    }

    private void registerDataPrefix() {
        Log.d(TAG, "registering data prefix for memory content cache...");
        try {
            //face.registerPrefix(dataPrefix, OnDataInterest,
            //        OnDataPrefixRegisterFailed, OnDataPrefixRegisterSuccess);
            m_sentData.setFace(m_face);
            m_userCertificateDataPackets.setFace(m_face);

            m_sentData.registerPrefix(m_dataPrefix, OnDataPrefixRegisterFailed, OnDataPrefixRegisterSuccess,
                    OnDataNotFoundSentData);
            m_userCertificateDataPackets.registerPrefix(
                    m_dataPrefix.getSubName(0, m_dataPrefix.size() - 1),
                    OnDataPrefixRegisterFailed, OnDataPrefixRegisterSuccess, OnDataNotFoundUserCertificateDataPackets);
        } catch (IOException | SecurityException e) {
            // should also be handled in callback, but in just in case...
            raiseError("exception registering data prefix", ErrorCode.NFD_PROBLEM, e);

        }

    }

    private void setUpChronoSync() {
        try {
            sync = new ChronoSync2013Custom(OnReceivedChronoSyncState, OnChronoSyncInitialized,
                    m_dataPrefix, m_broadcastPrefix, m_session, m_face, m_keyChain,
                    m_keyChain.getDefaultCertificateName(), SYNC_LIFETIME,
                    OnBroadcastPrefixRegisterFailed);
        } catch (IOException | SecurityException e) {
            // should also be handled in callback, but in just in case...
            sendNFDFailureNotification();
            raiseError("exception setting up ChronoSync", ErrorCode.NFD_PROBLEM, e);
            restartNetworkThread();
        }
    }

    protected abstract void doApplicationSetup();

    private void sendInitialMessage() {
        ArrayList<byte[]> certFragments = FragmentationHelpers.createFragments(m_myCert.wireEncode().getImmutableArray());
        send(certFragments);
    }

    protected void raiseError(String logMessage, ErrorCode code, Throwable exception) {
        if (exception == null) Log.e(TAG, logMessage);
        else Log.e(TAG, logMessage, exception);
        if (m_raisedErrorCode == null) m_raisedErrorCode = code;
        stopNetworkThread();
    }

    protected void raiseError(String logMessage, ErrorCode code) {
        raiseError(logMessage, code, null);
    }

    private void handleAnyRaisedError() {
        if (m_raisedErrorCode == null) return;

        stopSelf();

        Log.d(TAG, "broadcasting error intent w/code = " + m_raisedErrorCode + "...");
        Intent bcast = new Intent(BCAST_ERROR);
        bcast.putExtra(EXTRA_ERROR_CODE, m_raisedErrorCode);
        LocalBroadcastManager.getInstance(ChronoSyncService.this).sendBroadcast(bcast);
    }

    protected void send(ArrayList<byte[]> audioClip) {
        Log.d(TAG, "SENTDATA GOT ADDED TO IN THE SEND FUNCTION");

        if (audioClip.get(0).length > 0 && audioClip.get(0)[0] == 42 && audioClip.get(0)[1] == 43) {
            m_currentSequenceNumber++;
            return;
        }

        if (audioClip.size() > 255) {
            Log.d(TAG, "AUDIOCLIP WAS MORE THAN 255 FRAGMENTS, NOT SENDING IT");
            return;
        }

        int numFragments = audioClip.size();

        for (int i = 0; i < numFragments; i++) {
            Data audioFragmentDataPacket = new Data();
            audioFragmentDataPacket.setName(new Name(m_dataPrefix +
                    "/" + Integer.toString(m_currentSequenceNumber)));
            audioFragmentDataPacket.getName().append(new byte[]{1}, ComponentType.GENERIC);
            audioFragmentDataPacket.getName().appendSegment(i);
            Log.d(TAG, "Adding a data packet with this name into memory content cache: " +
                    audioFragmentDataPacket.getName().toString());
            Blob audioFragmentBlob;
            audioFragmentBlob = new Blob(audioClip.get(i));
            Name finalBlockIdName = new Name();
            finalBlockIdName.appendSegment(numFragments - 1);
            audioFragmentDataPacket.getMetaInfo().setFinalBlockId(finalBlockIdName.get(0));
            audioFragmentDataPacket.setContent(audioFragmentBlob);

            try {
                m_keyChain.sign(audioFragmentDataPacket);
            } catch (SecurityException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

            Log.d(TAG, "Hex of certificate fragment: " + audioFragmentDataPacket.getContent().toHex());
            if (Integer.parseInt(audioFragmentDataPacket.getName().get(-3).getValue().toString()) == 0) {
                // the first data published is actually the user's certificate
                Log.d(TAG, "Inserting this fragment into user certificate data packets: " + audioFragmentDataPacket.getName().toString());
                m_userCertificateDataPackets.add(audioFragmentDataPacket);
            }

            m_sentData.add(audioFragmentDataPacket);

        }
        m_currentSequenceNumber++;
    }

    private void publishSeqNumsIfNeeded() {
        if (!m_syncInitialized) return;
        while (nextSyncSeqNum() < nextDataSeqNum()) {
            long seqNumToPublish = nextSyncSeqNum();
            try {
                sync.publishNextSequenceNo();
                Log.d(TAG, "published seqnum " + seqNumToPublish);
                sendPublishNotification(seqNumToPublish);
            } catch (IOException | SecurityException e) {
                raiseError("failed to publish seqnum " + seqNumToPublish, ErrorCode.NFD_PROBLEM, e);
            }
        }
    }

    private void processSyncState(long syncSession, long syncSeqNum, String syncDataPrefix, String userId, boolean isRecovery) {

        Log.d(TAG, "ENTERED THE PROCESS SYNC STATE FUNCTION IN CHRONOSYNC SERVICE");

        Log.d(TAG, "userId for received sync state: " + userId);

        Log.d(TAG, "Proceeding to process sync state with seq num " + syncSeqNum + " for user " + userId);

        Log.d(TAG, "received" + (isRecovery ? " RECOVERY " : " ") + "sync state for " +
                syncDataPrefix + "/" + syncSession + "/" + syncSeqNum);

        if (syncDataPrefix.equals(m_dataPrefix.toString())) {
            Log.d(TAG, "ignoring sync state for own user");
            return;
        }

        if (m_nextSeqNumToRequest.get(syncDataPrefix) == null && !m_shouldRetrieveStaleData) {
            Log.d(TAG, "preventing retrieval of stale data");
            m_nextSeqNumToRequest.put(syncDataPrefix, syncSeqNum + 1); // skip requesting seqnum again
        }

        requestMissingSeqNums(syncDataPrefix, syncSeqNum);

        if (syncSeqNum != 0) {
            String msg = "Sending a sync notification receiver thing for seq " + syncSeqNum + ".";
            if (isRecovery)
                msg += " WAS RECOVERY TOO";
            Log.d(TAG, msg);

        }
    }

    private void requestMissingSeqNums(String syncDataId, long availableSeqNum) {
        Long seqNumToRequest = m_nextSeqNumToRequest.get(syncDataId);
        if (seqNumToRequest == null) seqNumToRequest = 0L;
        while (availableSeqNum >= seqNumToRequest) {
            String missingDataNameStr = syncDataId + "/" + seqNumToRequest;
            String userId = syncDataId.substring(syncDataId.lastIndexOf("/") + 1);
            Name missingDataName = new Name(missingDataNameStr);
            Log.d(TAG, "requesting missing seqnum " + seqNumToRequest);
            seqNumToRequest++;
            Interest baseInterest = new Interest();
            baseInterest.setName(missingDataName);
            if (Integer.parseInt(missingDataName.get(-1).getValue().toString()) == 0) {
                Log.d(TAG, "Data fetcher is being called to retrieve a certificate with this base name: " + baseInterest.getName().toString());
                m_dataFetcher.fetch(m_face, baseInterest, SegmentFetcherCustom.DontVerifySegment, onReceivedCertificate, onCertificateRetrievalError,
                        onSegmentFetchedAndVerified,m_maxReattemptsPerSegmentInterest, m_segmentInterestTimeout);
            }
            else {
                Log.d(TAG, "Data fetcher is being called to retrieve a regular data with name: " + baseInterest.getName().toString());
                m_dataFetcher.fetch(m_face, baseInterest, verifySegment, onReceivedFullAudio, onError,
                        onSegmentFetchedAndVerified, m_maxReattemptsPerSegmentInterest, m_segmentInterestTimeout);
                sendStartedFetchingNotification(userId, seqNumToRequest - 1, m_userNames.get(userId));
            }
        }
        m_nextSeqNumToRequest.put(syncDataId, seqNumToRequest);
    }

    private int nextDataSeqNum() {
        return m_currentSequenceNumber;
    }

    private long nextSyncSeqNum() {
        return sync.getSequenceNo() + 1;
    }

    protected abstract void handleApplicationData(byte[] receivedData, String userId, long seqNum);

    protected abstract void sendSyncNotification(String userId, long seqNum, String userName);

    protected abstract void sendStartedFetchingNotification(String userId, long seqNum, String userName);

    protected abstract void sendFinishedFetchingNotification(String userId, long seqNum, String userName, long numSegments);

    protected abstract void sendFailedFetchingNotification(String userId, long seqNum, String userName);

    protected abstract void sendPublishNotification(long seqNum);

    protected abstract void sendNFDFailureNotification();

    protected abstract void sendNFDRecoveryNotification();

    protected abstract void sendWifiFailureNotification();

    protected abstract void sendNameForUserIdNotification(String userId, String name);

    protected abstract void sendNFDCRelatedMessage(String message);

    /***** Callbacks for NDN network thread *****/

    private final OnInterestCallback OnDataNotFoundSentData = new OnInterestCallback() {
        @Override
        public void onInterest(Name prefix, Interest interest, Face face, long interestFilterId,
                               InterestFilter filterData) {
            Name interestName = interest.getName();

            if (!interest.getName().get(-3).getValue().toString().equals("sync"))
                Log.d(TAG, "Sent data memory content cache was unable to satisfy interest for: " + interestName);
        }
    };

    private final OnInterestCallback OnDataNotFoundUserCertificateDataPackets = new OnInterestCallback() {
        @Override
        public void onInterest(Name prefix, Interest interest, Face face, long interestFilterId,
                               InterestFilter filterData) {
            Name interestName = interest.getName();

            if (!interest.getName().get(-3).getValue().toString().equals("sync"))
                Log.d(TAG, "User certificate data packets memory content cache was unable to satisfy interest for: " + interestName);
        }
    };

    private final ChronoSync2013Custom.OnReceivedSyncState OnReceivedChronoSyncState =
            new ChronoSync2013Custom.OnReceivedSyncState() {
                @Override
                public void onReceivedSyncState(List syncStates, boolean isRecovery) {
                    Log.d(TAG, "sync states received");
                    for (Object syncState : syncStates) {
                        PrioritizedSyncStateInfo info = new PrioritizedSyncStateInfo((ChronoSync2013Custom.SyncState) syncState, isRecovery);
                        long syncSession = info.syncState.getSessionNo(),
                                syncSeqNum = info.syncState.getSequenceNo();
                        String syncDataPrefix = info.syncState.getDataPrefix(),
                                syncDataId = syncDataPrefix; //+ "/" + syncSession;
                        String userId = syncDataId.substring(syncDataId.lastIndexOf("/") + 1);

                        if (syncDataPrefix.equals(m_dataPrefix.toString())) {
                            Log.d(TAG, "ignoring sync state for own user");
                            continue;
                        }

                        if (!m_userSyncStates.containsKey(userId)) {
                            m_userSyncStates.put(userId, new UserBufferedSyncStateInfosManager());
                        }

                        m_userSyncStates.get(userId).addSyncStateInfoToSyncStateInfosBuffer(info);

                        Log.d(TAG, "Calling send sync notification.");
                        sendSyncNotification(userId, syncSeqNum, m_userNames.get(userId));

                        //processSyncState((ChronoSync2013Custom.SyncState) syncState, isRecovery);
                    }
                    Log.d(TAG, "finished processing " + syncStates.size() + " sync states");
                }
            };

    private final ChronoSync2013Custom.OnInitialized OnChronoSyncInitialized =
            new ChronoSync2013Custom.OnInitialized() {
                @Override
                public void onInitialized() {
                    Log.d(TAG, "ChronoSync initialization complete; seqnum is now " +
                            sync.getSequenceNo());
                    // Ensure that sentData is in sync with the initial seqnum
                    while (nextDataSeqNum() < nextSyncSeqNum()) {
                        //Log.d(TAG, "SENTDATA GOT ADDED TO IN ONCHRONOSYNCINITIALIZED");
                        //m_sentData.add(null);
                        m_currentSequenceNumber++;
                    }
                    m_syncInitialized = true;
                }
            };

    private final OnRegisterSuccess OnDataPrefixRegisterSuccess = new OnRegisterSuccess() {
        @Override
        public void onRegisterSuccess(Name prefix, long registeredPrefixId) {
            Log.d(TAG, "successfully registered data prefix: " + prefix);
        }
    };

    private final OnRegisterFailed OnDataPrefixRegisterFailed = new OnRegisterFailed() {
        @Override
        public void onRegisterFailed(Name prefix) {
            raiseError("failed to register application prefix " + prefix.toString(),
                    ErrorCode.NFD_PROBLEM);
        }
    };

    private final OnRegisterFailed OnBroadcastPrefixRegisterFailed = new OnRegisterFailed() {
        @Override
        public void onRegisterFailed(Name prefix) {
            raiseError("failed to register broadcast prefix " + prefix.toString(),
                    ErrorCode.NFD_PROBLEM);
        }
    };

    private SegmentFetcherCustom.OnSegmentFetchedAndVerified onSegmentFetchedAndVerified = new SegmentFetcherCustom.OnSegmentFetchedAndVerified() {
        @Override
        public void onSegmentFetchedAndVerified(Name segmentName, long currentSegmentNumber, long finalSegmentNumber, int segmentPayloadSize) {

        }
    };

    private SegmentFetcherCustom.OnComplete onReceivedFullAudio = new SegmentFetcherCustom.OnComplete() {
        @Override
        public void onComplete(Blob fullContent, Data lastDataPacket, Name baseDataName, long numSegments) {
            Log.d(TAG, "Base data name of full audio clip retrieved:" + baseDataName.toString());
            String userId = baseDataName.get(-2).getValue().toString();
            long seqNum = Long.parseLong(baseDataName.get(-1).getValue().toString());

            handleApplicationData(fullContent.getImmutableArray(), userId, seqNum);

            sendFinishedFetchingNotification(userId, seqNum, m_userNames.get(userId), numSegments);
        }
    };

    private SegmentFetcherCustom.OnComplete onReceivedCertificate = new SegmentFetcherCustom.OnComplete() {
        @Override
        public void onComplete(Blob fullContent, Data lastDataPacket, Name baseDataName, long numSegments) {
            // this assumes that a certificate will never exceed the full NDN packet length
            Blob content = lastDataPacket.getContent();
            Log.d(TAG, "Base data name of certificate retrieved:" + baseDataName.toString());
            String userId = baseDataName.get(-2).getValue().toString();
            long seqNum = Long.parseLong(baseDataName.get(-1).getValue().toString());

            sendFinishedFetchingNotification(userId, seqNum, m_userNames.get(userId), numSegments);

            Log.d(TAG, "Got a certificate from userid: " + userId);
            Log.d(TAG, "Hex of certificate bytes: " + content.toHex());
            String userName = "";
            CertificateV2 receivedCert = null;
            Data contentData = null;
            try {
                contentData = new Data();
                contentData.wireDecode(content);
                receivedCert = new CertificateV2(contentData);
                if (!VerificationHelpers.verifyDataSignature(contentData, receivedCert)) {
                    Log.e(TAG, "Failed to verify the certificate we received from user id: " + userId);
                    Log.d(TAG, "Data fetcher is being called for failed verification to retrieve a certificate with this base name: " + baseDataName.toString());
                    m_dataFetcher.fetch(m_face, new Interest(baseDataName), SegmentFetcherCustom.DontVerifySegment,
                            onReceivedCertificate, onCertificateRetrievalError, onSegmentFetchedAndVerified,
                        m_maxReattemptsPerSegmentInterest, m_segmentInterestTimeout);
                    return;
                }
                userName = receivedCert.getKeyName().get(-3).getValue().toString();
                Log.d(TAG, "User name of userid " + userId + ": " + userName);
            } catch (CertificateV2.Error error) {
                Log.e(TAG, "Failed to create certificate from blob from user: " + userId);
                error.printStackTrace();
                Log.d(TAG, "Data fetcher is being called for failed blob to retrieve a certificate with this base name: " + baseDataName.toString());
                m_dataFetcher.fetch(m_face, new Interest(baseDataName), SegmentFetcherCustom.DontVerifySegment,
                        onReceivedCertificate, onCertificateRetrievalError, onSegmentFetchedAndVerified,
                        m_maxReattemptsPerSegmentInterest, m_segmentInterestTimeout);
                return;
            } catch (EncodingException e) {
                e.printStackTrace();
                Log.d(TAG, "Data fetcher is being called for encoding exception to retrieve a certificate with this base name: " + baseDataName.toString());
                m_dataFetcher.fetch(m_face, new Interest(baseDataName), SegmentFetcherCustom.DontVerifySegment,
                        onReceivedCertificate, onCertificateRetrievalError, onSegmentFetchedAndVerified,
                        m_maxReattemptsPerSegmentInterest, m_segmentInterestTimeout);
                return;
            }
            m_userNames.put(userId, userName);
            m_userCertificates.put(userId, receivedCert);
            Log.d(TAG, "Inserting this data packet into user certificate data packets: " + lastDataPacket.getName().toString());
            m_userCertificateDataPackets.add(lastDataPacket);

            sendNameForUserIdNotification(userId, userName);

        }
    };

    private SegmentFetcherCustom.VerifySegment verifySegment = new SegmentFetcherCustom.VerifySegment() {
        @Override
        public boolean verifySegment(Data data) {
            KeyLocator keyLocator = KeyLocator.getFromSignature(data.getSignature());
            String userId = data.getName().get(-4).getValue().toString();

            Log.d(TAG, "Got data segment with key locator name: " + keyLocator.getKeyName().toUri() +
                    ", user id: " + userId);

            CertificateV2 cert = m_userCertificates.get(userId);
            if (cert == null) {
                Log.e(TAG, "Failed to get certificate for user id: " + userId);
                return false;
            }

            if (!VerificationHelpers.verifyDataSignature(data, cert)) {
                Log.e(TAG, "Failed to verify data segment with name : " + data.getName());
                return false;
            }

            return true;
        }
    };

    private SegmentFetcherCustom.OnError onError = new SegmentFetcherCustom.OnError() {
        @Override
        public void onError(SegmentFetcherCustom.ErrorCode errorCode, String message, Name name) {
            Log.d(TAG, "Segment fetcher failed to fetch segment.");
            Log.d(TAG, "Error message: " + message);
            Log.d(TAG, "Error code: " + errorCode.toString());


            Intent errorIntent = new Intent(ACTION_FAILED_RETRIEVAL);
            String[] failureInfo = new String[4];
            failureInfo[SEQ_NUM] = name.get(-1).getValue().toString();
            failureInfo[USER_ID] = name.get(-2).getValue().toString();
            failureInfo[USER_NAME] = m_userNames.get(failureInfo[USER_ID]);
            failureInfo[ERROR_CODE] = errorCode.toString();
            errorIntent.putExtra(EXTRA_FAILURE_INFO, failureInfo);

            LocalBroadcastManager.getInstance(MainActivity.getInstance()).sendBroadcast(errorIntent);

            sendFailedFetchingNotification(failureInfo[USER_ID], Long.parseLong(failureInfo[SEQ_NUM]), failureInfo[USER_NAME]);

        }
    };

    private SegmentFetcherCustom.OnError onCertificateRetrievalError = new SegmentFetcherCustom.OnError() {
        @Override
        public void onError(SegmentFetcherCustom.ErrorCode errorCode, String message, Name name) {
            Log.d(TAG, "Segment fetcher failed to fetch segment.");
            Log.d(TAG, "Error message: " + message);
            Log.d(TAG, "Error code: " + errorCode.toString());

            String[] failureInfo = new String[4];
            failureInfo[SEQ_NUM] = name.get(-1).getValue().toString();
            failureInfo[USER_ID] = name.get(-2).getValue().toString();
            failureInfo[USER_NAME] = m_userNames.get(failureInfo[USER_ID]);
            failureInfo[ERROR_CODE] = errorCode.toString();

            Log.d(TAG, "Failed to fetch certificate for user id " + failureInfo[USER_ID]
                    + ", user name " + failureInfo[USER_NAME]);
            Log.d(TAG, "Data fetcher is being called for timeout to retrieve a certificate with this base name: " + name.toString());
            m_dataFetcher.fetch(m_face, new Interest(name), SegmentFetcherCustom.DontVerifySegment,
                    onReceivedCertificate, onCertificateRetrievalError, onSegmentFetchedAndVerified,
                    m_maxReattemptsPerSegmentInterest, m_segmentInterestTimeout);

        }
    };


    public static IntentFilter getIntentFilter() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_FAILED_RETRIEVAL);

        return filter;
    }

    public static ChronoSyncService getInstance() {
        return chronoSyncService;
    }
}