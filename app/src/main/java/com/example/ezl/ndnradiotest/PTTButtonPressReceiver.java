package com.example.ezl.ndnradiotest;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

public class PTTButtonPressReceiver extends BroadcastReceiver {

    private final String TAG = "PTTButtonPressReceiver";

    public static String ACTION_PTT_KEY_DOWN = "ACTION_PTT_KEY_DOWN";
    public static String ACTION_PTT_KEY_UP = "ACTION_PTT_KEY_UP";

    @Override
    public void onReceive(Context context, Intent intent) {
        if (MainActivity.getInstance() != null) {
            if (intent.getAction().equals("com.sonim.intent.action.PTT_KEY_DOWN")) {
                //Toast.makeText(MainActivity.getInstance(), "ptt key down", Toast.LENGTH_SHORT).show();
                Intent pttButtonPressedIntent = new Intent(ACTION_PTT_KEY_DOWN);
                LocalBroadcastManager.getInstance(MainActivity.getInstance()).sendBroadcast(pttButtonPressedIntent);
            } else if (intent.getAction().equals("com.sonim.intent.action.PTT_KEY_UP")) {
                //Toast.makeText(MainActivity.getInstance(), "ptt key up", Toast.LENGTH_SHORT).show();
                Intent pttButtonPressedIntent = new Intent(ACTION_PTT_KEY_UP);
                LocalBroadcastManager.getInstance(MainActivity.getInstance()).sendBroadcast(pttButtonPressedIntent);
            } else {
                Log.d(TAG, "Some other unexpected intent came from ptt button listener: " + intent.getAction());
            }
        }
    }

    public static IntentFilter getIntentFilter() {
        IntentFilter ret = new IntentFilter();
        ret.addAction(ACTION_PTT_KEY_DOWN);
        ret.addAction(ACTION_PTT_KEY_UP);

        return ret;
    }
}


