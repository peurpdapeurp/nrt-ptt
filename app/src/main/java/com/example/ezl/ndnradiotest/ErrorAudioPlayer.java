package com.example.ezl.ndnradiotest;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.io.File;
import java.io.IOException;

public class ErrorAudioPlayer {

    private final String TAG = "ErrorAudioPlayer";

    boolean m_currently_playing_audio = false;
    private MediaPlayer m_player = null;
    private String m_filePath = "";

    ErrorAudioPlayer(String filePath) {
        m_filePath = filePath;
    }

    public void play(String fileName) {

        if (m_currently_playing_audio) {
            stopPlaying();
        }

        m_player = new MediaPlayer();
        try {
            File test = new File(m_filePath + "/" + fileName + ".wav");

            if (!test.exists()) {

                Log.e(TAG, "Did not find the failure rejection wav file.");

                sendErrorMessageFinishedPlayingIntent();

                return;
            }

            m_player.setDataSource(m_filePath + "/" + fileName + ".wav");
            m_player.prepare();
            m_player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                @Override
                public void onCompletion(MediaPlayer mp) {
                    Log.d(TAG, "Finished playing an audio clip.");

                    m_currently_playing_audio = false;

                    sendErrorMessageFinishedPlayingIntent();

                    m_player.release();
                }

            });
            m_player.start();
            m_currently_playing_audio = true;
        } catch (IOException e) {
            m_currently_playing_audio = false;
            Log.e(TAG, "prepare() failed for error message");
            if (!m_currently_playing_audio)
                sendErrorMessageFinishedPlayingIntent();
        }

    }

    private void sendErrorMessageFinishedPlayingIntent() {

        Intent errorMessageFinishedPlayingIntent = new Intent(BufferedAudioPlayer.ERROR_MESSAGE_PLAYER_SIGNAL_END);

        LocalBroadcastManager.getInstance(MainActivity.getInstance()).sendBroadcast(errorMessageFinishedPlayingIntent);

    }

    public boolean isPlaying() {
        return m_currently_playing_audio;
    }

    public void stopPlaying() {

        Log.d(TAG, "Prematurely stopped playing an audio clip.");

        m_currently_playing_audio = false;

        if (m_player != null) {
            m_player.release();
            m_player = null;
        }
    }
}
