package com.example.ezl.ndnradiotest;

import android.util.Log;

import java.util.ArrayList;

import static net.named_data.jndn.Node.getMaxNdnPacketSize;

public class FragmentationHelpers {

    public static final int MAX_DATA_PAYLOAD = getMaxNdnPacketSize() - 500;

    private static final String TAG = "FragmentationHelpers";

    public static byte getNumberOfFragments(int arrayLength) {

        if (arrayLength > 255 * MAX_DATA_PAYLOAD) {
            Log.d(TAG, "NUNBER OF FRAGMENTS WAS TOO LARGE TO FIT INTO ONE BYTE, RETURNING -1");
            return -1;
        }

        int numFragments = arrayLength / MAX_DATA_PAYLOAD;
        if (arrayLength % MAX_DATA_PAYLOAD != 0) {
            numFragments++;
        }

        return (byte) numFragments;
    }

    public static int getArrayLengthFromFragments(int numFragments) {
        return numFragments * MAX_DATA_PAYLOAD;
    }

    public static ArrayList<byte[]> createFragments(byte[] data) {

        ArrayList<byte[]> fragments = new ArrayList<>();
        int maxNdnPacketSize = MAX_DATA_PAYLOAD;

        if (data.length < maxNdnPacketSize) {
            fragments.add(data);
            return fragments;
        }

        byte numFragments = getNumberOfFragments(data.length);
        Log.d(TAG, "Got " + numFragments + " for the number of fragments.");

        if (numFragments == -1) {
            Log.d(TAG, "NUMBER OF FRAGMENTS WAS TOO LARGE, NOT CREATING ARRAYLIST");
            return null;
        }

        for (int i = 0; i < numFragments - 1; i++) {
            byte[] currentFragmentBytes = new byte[maxNdnPacketSize];
            System.arraycopy(data, maxNdnPacketSize*i, currentFragmentBytes, 0, maxNdnPacketSize);
            fragments.add(currentFragmentBytes);
        }

        int remainingBytesLength = data.length - (numFragments - 1) * maxNdnPacketSize;
        byte[] remainingBytes = new byte[remainingBytesLength];

        System.arraycopy(data, maxNdnPacketSize * (numFragments - 1), remainingBytes,
                0, remainingBytesLength);

        fragments.add(remainingBytes);

        return fragments;
    }
}
