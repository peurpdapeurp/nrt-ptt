package com.example.ezl.ndnradiotest;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.util.concurrent.PriorityBlockingQueue;

public class UserBufferedSyncStateInfosManager {

    private final String TAG = "UsrBfrdSyncInfosMngr";

    private PriorityBlockingQueue<PrioritizedSyncStateInfo> m_bufferedReceivedSyncStates;
    boolean m_currently_processing_sync_state_infos;

    UserBufferedSyncStateInfosManager() {
        m_bufferedReceivedSyncStates = new PriorityBlockingQueue<>();
        processBufferedSyncStateInfosThread.start();
    }

    public void processNextAvailableSyncStateInfo() {

        Log.d(TAG, "POLL WAS CALLED ON BUFFERED SyncStateInfos IN processNextAvailableSyncStateInfo");
        PrioritizedSyncStateInfo info = m_bufferedReceivedSyncStates.poll();

        if (info == null && m_bufferedReceivedSyncStates.size() == 0) {
            Log.d(TAG, "Finished processing all buffered sync state infos.");
            return;
        }
        else if (info == null) {
            Log.d(TAG, "Polling sync states queue returned null but queue size isnt zero, " +
                    "something is fishy...");
            return;
        }

        if (info != null) {
            Intent syncStateIntent = new Intent(ChronoSyncService.SYNC_STATE_FROM_USER_SYNC_STATE_INFO_MANAGER);

            long syncSession = info.syncState.getSessionNo(),
                    syncSeqNum = info.syncState.getSequenceNo();
            String syncDataPrefix = info.syncState.getDataPrefix(),
                    userId = syncDataPrefix.substring(syncDataPrefix.lastIndexOf("/") + 1);


            //if (syncSeqNum != 0 && !MainActivity.getInstance().m_nameAudioBytes.containsKey(userId)) {
            /*
            if (syncSeqNum != 0 && !ChronoSyncService.getInstance().m_userCertificates.containsKey(userId)) {

                Log.d(TAG, "Did not yet receive the certificate for user " + userId + ", " +
                        "skipping sync state with priority " + info.priority);

                m_bufferedReceivedSyncStates.add(info);

                m_currently_processing_sync_state_infos = false;

                return;
            }
            */


            long[] longInfo = new long[2];
            longInfo[ChronoSyncService.SYNC_STATE_INFO_SYNC_SESSION] = syncSession;
            longInfo[ChronoSyncService.SYNC_STATE_INFO_SEQ_NUM] = syncSeqNum;

            String[] stringInfo = new String[2];
            stringInfo[ChronoSyncService.SYNC_STATE_INFO_DATA_PREFIX] = syncDataPrefix;
            stringInfo[ChronoSyncService.SYNC_STATE_INFO_USER_ID] = userId;

            boolean isRecovery = info.isRecovery;

            syncStateIntent.putExtra(ChronoSyncService.EXTRA_SYNC_STATE_INFO_LONGS, longInfo);
            syncStateIntent.putExtra(ChronoSyncService.EXTRA_SYNC_STATE_INFO_STRINGS, stringInfo);
            syncStateIntent.putExtra(ChronoSyncService.EXTRA_SYNC_STATE_INFO_IS_RECOVERY, isRecovery);

            LocalBroadcastManager.getInstance(ChronoSyncService.getInstance()).sendBroadcast(syncStateIntent);

            m_currently_processing_sync_state_infos = false;
        }
        else {
            m_currently_processing_sync_state_infos = false;
            Log.d(TAG, "Finished processing all buffered sync state infos.");
        }
    }

    public void addSyncStateInfoToSyncStateInfosBuffer(PrioritizedSyncStateInfo info) {
        Log.d(TAG, "Adding a sync state info to sync state infos buffer");
        m_bufferedReceivedSyncStates.add(info);

    }

    Thread processBufferedSyncStateInfosThread = new Thread(new Runnable() {

        public void run() {
            while (true) {
                int currentSize = m_bufferedReceivedSyncStates.size();
                if (currentSize != 0) {

                    Log.d(TAG, "buffered sync state infos thread noticed we received new sync state info");

                    if (!m_currently_processing_sync_state_infos) {
                        m_currently_processing_sync_state_infos = true;
                        Log.d(TAG, "Making initial call to process next available sync state info");
                        processNextAvailableSyncStateInfo();
                    }
                }
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

    });
}
