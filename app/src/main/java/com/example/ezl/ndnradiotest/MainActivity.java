package com.example.ezl.ndnradiotest;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.media.MediaCodec;
import android.media.MediaFormat;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.SupplicantState;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.LinkedBlockingQueue;

import static com.example.ezl.ndnradiotest.FragmentationHelpers.getNumberOfFragments;
import static com.example.ezl.ndnradiotest.FragmentationHelpers.createFragments;
import static org.apache.commons.io.FileUtils.readFileToByteArray;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "AudioRecordTest";
    private static final int REQUEST_RECORD_AUDIO_PERMISSION = 200;
    public static final int HEARTBEAT_LIFETIME = 12000;
    public static final int MAX_HEARTBEAT_RANDOMIZER = 3000;

    public static MainActivity mainActivity;

    private String m_cache_path = null;

    private Button m_button_ptt;
    private Button m_button_play_last_recorded;
    private Button m_button_play_last_received;

    public static TextView m_user_prefix_display = null;
    public static TextView m_broadcast_prefix_display = null;
    private TextView m_name_display = null;
    private TextView m_segment_interest_lifetime_display = null;
    private TextView m_segment_interest_max_reattempts_display = null;

    private TextView m_action_text = null;
    private TextView m_last_size_display = null;
    //public TextView m_log_text = null;

    private NDNRadioService m_radio_service = null;

    private String absolutePath;
    private String m_channel = null;
    private String m_name = null;
    private String m_segment_interest_lifetime;
    private String m_segment_interest_max_reattempts;
    private String m_ap_ip_address;

    private BufferedAudioPlayer m_bufferedAudioPlayer;
    private ErrorAudioPlayer m_errorAudioPlayer;
    private LocalAudioManager m_audioManager;

    ConcurrentHashMap<String, UserAudioClips> m_userAudioBuffers;

    // Requesting permission to RECORD_AUDIO
    private boolean permissionToRecordAccepted = false;
    private String[] permissions = {Manifest.permission.RECORD_AUDIO};

    private BlockingQueue<String> m_failureFileNames;
    private BlockingQueue<String> m_syncAudioNotificationNames;

    private BlockingQueue<GeneratedMessageInfo> m_failureGenerationInfos;
    private BlockingQueue<GeneratedMessageInfo> m_syncAudioGenerationInfos;

    private BlockingQueue<UserIdAndName> m_namesToGenerate;
    public ConcurrentHashMap<String, byte[]> m_nameAudioBytes;

    private BlockingQueue<String> m_logMessages;
    long m_startUpTime;

    final String m_nfdFailureUtteranceId = "nfdFailure";
    final String m_wifiFailureUtteranceId = "wifiFailure";
    final String m_nfdRecoveryUtteranceId = "nfdRecovery";
    final String m_nfdFailureRejectionUtteranceId = "nfdFailureRejection";
    File m_nfdFailureFile;
    File m_nfdRecoveryFile;
    File m_wifiFailureFile;
    File m_nfdFailureRejectionFile;
    byte[] m_nfdFailureMessageBytes;
    byte[] m_wifiFailureMessageBytes;
    byte[] m_nfdRecoveryMessageBytes;
    byte[] m_nfdFailureRejectionMessageBytes;

    boolean m_nfdHasFailed = false;

    private Button test_button;

    private TextToSpeech m_message_failure_tts;
    private TextToSpeech m_sync_tts;
    private TextToSpeech m_name_tts;
    private TextToSpeech m_temp_tts;

    Timer m_timer;
    final int M_BUTTON_PRESS_TIME_LIMIT = 1500;
    boolean m_button_pressable = true;

    volatile boolean m_currently_generating_sync_audio = false;
    volatile boolean m_currently_generating_retrieval_failure_audio = false;
    volatile boolean m_currently_generating_name_audio = false;

    Timer m_sync_audio_generation_limit_timer;
    final int M_SYNC_AUDIO_GENERATION_TIME_LIMIT = 500;

    Timer m_retrieval_failure_audio_generation_limit_timer;
    final int M_RETRIEVAL_FAILURE_AUDIO_GENERATION_TIME_LIMIT = 300;

    Timer m_name_audio_generation_limit_timer;
    final int M_NAME_AUDIO_GENERATION_LIMIT_TIMER = 10000;

    public static final int MISSING_AUDIO_SEQ_WAIT_TIME = 10000;
    public static final int MISSING_SYNC_SEQ_WAIT_TIME = 1000;

    private final int duration = 150; // milliseconds
    private final int sampleRate = 8000;
    private final int numSamples = ((duration * sampleRate) / 1000);
    private final double sample[] = new double[numSamples];
    private final double freqOfTone = 440; // hz

    private final byte rawBeepSnd[] = new byte[2 * numSamples];
    private byte beepSnd[];

    ConnectivityManager connManager;
    NetworkInfo mWifi;
    // true if last state was connected, false if last state was disconnected
    boolean lastWifiConnectionState = false;

    BroadcastReceiver bufferedAudioPlayerAndErrorPlayerListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(BufferedAudioPlayer.ERROR_MESSAGE_PLAYER_APPROVE_START)) {
                Log.d(TAG, "got an approval from buffered audio player to start playing error message");
                String fileName = intent.getExtras().getString(BufferedAudioPlayer.EXTRA_FILE_NAME);
                m_errorAudioPlayer.play(fileName);
            } else if (intent.getAction().equals(BufferedAudioPlayer.ERROR_MESSAGE_PLAYER_SIGNAL_END)) {
                m_bufferedAudioPlayer.resume();
            }
        }
    };

    BroadcastReceiver receivedAudioListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(NDNRadioService.ACTION_GOT_AUDIO_BYTES)) {
                byte[] bytesOfAudio = intent.getByteArrayExtra(NDNRadioService.EXTRA_AUDIO_BYTES);
                String[] audioInfo = intent.getExtras().getStringArray(NDNRadioService.EXTRA_AUDIO_INFO);

                String userId = audioInfo[NDNRadioService.USER_ID];
                long seqNum = Long.parseLong(audioInfo[NDNRadioService.SEQ_NUM]);

                if (bytesOfAudio == null) {
                    Log.e(TAG, "PROBLEM, AUDIO BYTES WE RECEIVED WAS NULL");
                }

                String audioInfoPrint = "";
                audioInfoPrint += "Printing out audio info of received audio bytes:" + "\n";
                audioInfoPrint += "User id: " + userId + "\n";
                audioInfoPrint += "Sequence Num: " + seqNum + "\n";
                audioInfoPrint += "Size of audio bytes received:" + bytesOfAudio.length;
                Log.d(TAG, audioInfoPrint);

                if (!m_userAudioBuffers.containsKey(userId)) {
                    Log.d(TAG, "Added new user buffered audio manager for userId: " + userId);
                    m_userAudioBuffers.put(userId,
                            new UserAudioClips(
                                    new UserBufferedAudioManager(seqNum - 1, userId, MISSING_SYNC_SEQ_WAIT_TIME),
                                    new UserBufferedAudioManager(seqNum - 1, userId, MISSING_AUDIO_SEQ_WAIT_TIME))
                    );
                }



                UserBufferedAudioManager syncAudioClips = m_userAudioBuffers.get(userId).syncAudioClips;
                UserBufferedAudioManager voiceAndRetrievalFailureAudioClips = m_userAudioBuffers.get(userId).voiceAndRetrievalFailureAudioClips;

                /*
                while (syncAudioClips.getLastConsecutiveSeqNum()
                        < seqNum) {
                    Log.d(TAG, "Waiting for sync audio clip to be played before entering " +
                            " this voice audio clip to be played...");
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                */

                PrioritizedByteArray actualAudio = new PrioritizedByteArray(seqNum * 2, bytesOfAudio);

                Log.d(TAG, "String for user id in received audio listener: " + "|" + userId + "|" +
                "\n" + "Length of userId in received audio listener: " + userId.length());

                byte[] nameAudioBytes = new byte[] {};
                if (m_nameAudioBytes.containsKey(userId)) {
                    nameAudioBytes = m_nameAudioBytes.get(userId);
                }
                else {
                    Log.d(TAG, "Couldn't find name audio bytes for user id : " + userId + ", inserting empty byte array for name audio");
                }

                PrioritizedByteArray nameAudio = new PrioritizedByteArray((seqNum * 2) - 1, nameAudioBytes);

                voiceAndRetrievalFailureAudioClips.insertIntoQueue(nameAudio);
                voiceAndRetrievalFailureAudioClips.insertIntoQueue(actualAudio);

            } else if (intent.getAction().equals(NDNRadioService.ACTION_GOT_SYNC) ||
                    intent.getAction().equals(NDNRadioService.ACTION_FINISHED_FETCH) ||
                    intent.getAction().equals(NDNRadioService.ACTION_FAILED_FETCH) ||
                    intent.getAction().equals(NDNRadioService.ACTION_STARTED_FETCH)) {

                Log.d(TAG, "Got an intent for " + intent.getAction());

                String[] logInfo = intent.getExtras().getStringArray(NDNRadioService.EXTRA_LOGINFO);

                logMessage(logInfo, intent.getAction());

                /*
                // the -1 at the end is just the failtype, which is only used for failure message generation, so
                // there's just a placeholder -1
                GeneratedMessageInfo newMessageInfo = new GeneratedMessageInfo(userId, userName, seqNum, "");
                m_syncAudioGenerationInfos.add(newMessageInfo);
                */

            }
            else if (intent.getAction().equals(NDNRadioService.ACTION_PUBLISH)) {
                Log.d(TAG, "Got an intent for " + intent.getAction());

                long seqNum = intent.getExtras().getLong(NDNRadioService.EXTRA_SEQNUM);

                logPublishMessage(seqNum);
            }
            else if (intent.getAction().equals(NDNRadioService.ACTION_NFD_FAILURE_NOTIFICATION)) {

                Log.d(TAG, "Got notifciation of nfd failure");
                if (!m_nfdHasFailed) {
                    m_nfdHasFailed = true;
                    //m_bufferedAudioPlayer.addAudioToAudioClipsBuffer(m_nfdFailureMessageBytes);

                    m_bufferedAudioPlayer.interruptPlayingToPlayThisFile(m_nfdFailureUtteranceId);

                    m_logMessages.add(System.currentTimeMillis() + "_" + "NFD_FAILED");

                }

            } else if (intent.getAction().equals(NDNRadioService.ACTION_NFD_RECOVERY_NOTIFICATION)) {

                Log.d(TAG, "Got notification of nfd recovery");
                m_nfdHasFailed = false;

                //m_bufferedAudioPlayer.addAudioToAudioClipsBuffer(m_nfdRecoveryMessageBytes);
                m_bufferedAudioPlayer.interruptPlayingToPlayThisFile(m_nfdRecoveryUtteranceId);

                m_logMessages.add(System.currentTimeMillis() + "_" + "NFD_RECOVERED");

            } else if (intent.getAction().equals(NDNRadioService.ACTION_WIFI_FAILURE_NOTIFICATION)) {

                Log.d(TAG, "Got notification of wifi failure.");
                m_bufferedAudioPlayer.addAudioToAudioClipsBuffer(m_wifiFailureMessageBytes);

            } else if (intent.getAction().equals(NDNRadioService.ACTION_GOT_NAME_FOR_USERID)) {

                String[] info = intent.getExtras().getStringArray(NDNRadioService.EXTRA_USER_ID_NAME_INFO);

                String userId = info[NDNRadioService.NAME_FOR_ID_NOTIF_USER_ID];
                String userName = info[NDNRadioService.NAME_FOR_ID_NOTIF_NAME];

                m_namesToGenerate.add(new UserIdAndName(userId, userName));


            } else if (intent.getAction().equals(NDNRadioService.ACTION_NFDC_MESSAGE)) {
                String message = intent.getExtras().getString(NDNRadioService.EXTRA_NFDC_MESSAGE);
                //m_log_text.append(message + "\n" + "------------" + "\n");
            } else {
                Log.w(TAG, "receivedAudioListener got an intent whose action wasnt for audio");
                Log.w(TAG, "Intent's action: " + intent.getAction());
            }

        }
    };

    void PTT_BUTTON_DOWN_LOGIC() {

        if (!m_button_pressable) {
            Log.d(TAG, "BUTTON IS NOT PRESSABLE RIGHT NOW");
            return;
        }

        m_bufferedAudioPlayer.pause();

        m_button_pressable = false;
        startWaitTimer();

        if (channelAndNameAreSelected()) {
            if (!m_audioManager.isRecording()) {
                if (!m_nfdHasFailed) {
                    m_audioManager.startRecording();
                    m_action_text.setText("Started recording...");
                } else {
                    Log.d(TAG, "Not recording because nfd failed.");
                    m_bufferedAudioPlayer.interruptPlayingToPlayThisFile(m_nfdFailureRejectionUtteranceId);
                    return;
                }
            }
        } else {
            Toast.makeText(MainActivity.this, "Please select a channel before recording.", Toast.LENGTH_SHORT).show();
        }

    }

    void PTT_BUTTON_UP_LOGIC() {
        m_bufferedAudioPlayer.resume();

        if (m_nfdHasFailed) {
            Log.d(TAG, "Not sending because nfd failed.");
            if (!m_errorAudioPlayer.isPlaying())
                m_bufferedAudioPlayer.interruptPlayingToPlayThisFile(m_nfdFailureRejectionUtteranceId);
        } else {
            if (m_audioManager.isRecording()) {
                m_audioManager.stopRecording();
                m_action_text.setText("Stopped recording...");
                //WavToAndFromRawHelpers.concatenateFiles(m_cache_path, "name", "recording", "final");

                sendLastAudio();
            }
        }
    }

    BroadcastReceiver pttPressListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(PTTButtonPressReceiver.ACTION_PTT_KEY_DOWN)) {

                PTT_BUTTON_DOWN_LOGIC();

            } else if (intent.getAction().equals(PTTButtonPressReceiver.ACTION_PTT_KEY_UP)) {

                PTT_BUTTON_UP_LOGIC();

            } else {
                Log.w(TAG, "pttPressListener got weird intent with action: " +
                        intent.getAction());
            }
        }
    };

    BroadcastReceiver localAudioManagerListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(LocalAudioManager.ACTION_STARTED_PLAYING)) {
                m_button_play_last_recorded.setText(MainActivity.getInstance().getString(R.string.play_button_text_stop));
                m_button_play_last_received.setText(MainActivity.getInstance().getString(R.string.play_button_text_stop));
            } else if (intent.getAction().equals(LocalAudioManager.ACTION_STOPPED_PLAYING)) {
                m_button_play_last_recorded.setText(MainActivity.getInstance().getString(R.string.play_button_text_play));
                m_button_play_last_received.setText("Play Last Received Audio");
            } else if (intent.getAction().equals(LocalAudioManager.ACTION_STARTED_RECORDING)) {

            } else if (intent.getAction().equals(LocalAudioManager.ACTION_STOPPED_RECORDING)) {

            } else {
                Log.w(TAG, "localAudioManagerListener got weird intent with action: " +
                        intent.getAction());
            }
        }
    };

    BroadcastReceiver userBufferedAudioManagersListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(UserBufferedAudioManager.ACTION_REQUEST_PLAYING)) {
                byte[] audioBytes = intent.getExtras().getByteArray(UserBufferedAudioManager.EXTRA_AUDIO_BYTES);

                Log.d(TAG, "Main activity got request to play audiobytes of length: " + audioBytes.length);
                m_bufferedAudioPlayer.addAudioToAudioClipsBuffer(audioBytes);
            } else {
                Log.w(TAG, "userBufferedAudioMangersListener got weird intent with action: " +
                        intent.getAction());
            }
        }
    };


    BroadcastReceiver failureListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(ChronoSyncService.ACTION_FAILED_RETRIEVAL)) {
                Log.d(TAG, "Got failure intent with following failure info:");
                String[] failureInfo = intent.getExtras().getStringArray(ChronoSyncService.EXTRA_FAILURE_INFO);
                long seqNum = Long.parseLong(failureInfo[ChronoSyncService.SEQ_NUM]);
                String userId = failureInfo[ChronoSyncService.USER_ID];
                String userName = failureInfo[ChronoSyncService.USER_NAME];
                String errorCode = failureInfo[ChronoSyncService.ERROR_CODE];
                Log.d(TAG, "Sequence no: " + seqNum);
                Log.d(TAG, "User id: " + userId);
                Log.d(TAG, "User name: " + userName);
                Log.d(TAG, "Error code: " + errorCode);

                m_failureGenerationInfos.add(new GeneratedMessageInfo(userId, userName, seqNum,
                        errorCode));

            } else {
                Log.w(TAG, "failureListener got weird intent with action: " +
                        intent.getAction());
            }
        }
    };


    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.activity_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        mainActivity = this;

        m_startUpTime = System.currentTimeMillis();

        absolutePath = getExternalCacheDir().getAbsolutePath();

        LocalBroadcastManager.getInstance(this).registerReceiver(ndnRadioBroadcastReceiver,
                NDNRadioService.getIntentFilter());

        // Record to the external cache directory for visibility
        m_cache_path = getExternalCacheDir().getAbsolutePath();

        File lastRecordedAudioFile = new File(m_cache_path + "/" + "final.wav");
        File lastReceivedAudioFile = new File(m_cache_path + "/" + "last.wav");

        lastRecordedAudioFile.delete();
        lastReceivedAudioFile.delete();

        final String m_nfdFailureFileName = m_cache_path + "/" + m_nfdFailureUtteranceId + ".wav";
        final String m_wifiFailureFileName = m_cache_path + "/" + m_wifiFailureUtteranceId + ".wav";
        final String m_nfdRecoveryFileName = m_cache_path + "/" + m_nfdRecoveryUtteranceId + ".wav";
        final String m_nfdFailureRejectionFileName = m_cache_path + "/" + m_nfdFailureRejectionUtteranceId + ".wav";

        ActivityCompat.requestPermissions(this, permissions, REQUEST_RECORD_AUDIO_PERMISSION);

        m_button_ptt = (Button) findViewById(R.id.button_ptt);
        m_button_ptt.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_DOWN) {
                    PTT_BUTTON_DOWN_LOGIC();
                    m_button_ptt.setText(getString(R.string.ptt_button_down_text));
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    PTT_BUTTON_UP_LOGIC();
                    m_button_ptt.setText(getString(R.string.ptt_button_up_text));
                }

                return true;
            }
        });

        m_button_play_last_recorded = (Button) findViewById(R.id.button_play_last_recorded);
        m_button_play_last_recorded.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!m_audioManager.isPlaying()) {
                    if (!m_bufferedAudioPlayer.isPlayingBufferedAudio()) {
                        m_audioManager.startPlaying(m_cache_path + "/" + "final.wav");
                        m_button_play_last_recorded.setText(getString(R.string.play_button_text_stop));
                    }
                    else {
                        Toast.makeText(MainActivity.this, "Can't play last recorded " +
                                "audio while buffered audio player is playing.", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    m_audioManager.stopPlaying();
                    m_button_play_last_recorded.setText(getString(R.string.play_button_text_play));
                }
            }
        });

        m_button_play_last_received = (Button) findViewById(R.id.button_play_last_received);
        m_button_play_last_received.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!m_audioManager.isPlaying()) {
                    if (!m_bufferedAudioPlayer.isPlayingBufferedAudio()) {
                        m_audioManager.startPlaying(m_cache_path + "/" + "last.wav");
                        m_button_play_last_received.setText(getString(R.string.play_button_text_stop));
                    }
                    else {
                        Toast.makeText(MainActivity.this, "Can't play last received " +
                                "audio while buffered audio player is playing.", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    m_audioManager.stopPlaying();
                    m_button_play_last_received.setText(getString(R.string.play_button_text_play));
                }
            }
        });

        m_last_size_display = (TextView) findViewById(R.id.last_size_text);

        m_action_text = (TextView) findViewById(R.id.action_text);
        m_user_prefix_display = (TextView) findViewById(R.id.user_prefix_display);
        m_broadcast_prefix_display = (TextView) findViewById(R.id.broadcast_prefix_display);
        m_name_display = (TextView) findViewById(R.id.name_display);
        m_segment_interest_lifetime_display = (TextView) findViewById(R.id.segment_interest_lifetime_display);
        m_segment_interest_max_reattempts_display = (TextView) findViewById(R.id.segment_interest_max_reattempts_display);
        //m_log_text = (TextView) findViewById(R.id.log_text);

        if (!channelAndNameAreSelected()) {
            launchLoginActivity();
        }

        LocalBroadcastManager.getInstance(this).registerReceiver(receivedAudioListener,
                NDNRadioService.getIntentFilter());

        LocalBroadcastManager.getInstance(this).registerReceiver(pttPressListener,
                PTTButtonPressReceiver.getIntentFilter());
        LocalBroadcastManager.getInstance(this).registerReceiver(localAudioManagerListener,
                LocalAudioManager.getIntentFilter());
        LocalBroadcastManager.getInstance(this).registerReceiver(userBufferedAudioManagersListener,
                UserBufferedAudioManager.getIntentFilter());
        LocalBroadcastManager.getInstance(this).registerReceiver(failureListener,
                ChronoSyncService.getIntentFilter());
        LocalBroadcastManager.getInstance(this).registerReceiver(bufferedAudioPlayerAndErrorPlayerListener,
                BufferedAudioPlayer.getIntentFilter());

        m_bufferedAudioPlayer = new BufferedAudioPlayer(absolutePath);
        m_errorAudioPlayer = new ErrorAudioPlayer(absolutePath);

        m_audioManager = new LocalAudioManager(m_cache_path);

        m_userAudioBuffers = new ConcurrentHashMap<>();

        test_button = (Button) findViewById(R.id.test_button);
        test_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<byte[]> temp = new ArrayList<>();
                temp.add(new byte[]{42, 43});
                m_radio_service.sendMessage(temp);
            }
        });

        m_message_failure_tts = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int i) {
                if (i != TextToSpeech.ERROR) {
                    Log.d(TAG, "text to speech object for failure messages was successfully initialized.");
                    m_message_failure_tts.setLanguage(Locale.US);
                } else {
                    Log.e(TAG, "TEXT TO SPEECH OBJECT FOR FAILURE MESSAGES FAILED TO INITIALIZE");
                }
            }
        });

        m_message_failure_tts.setOnUtteranceProgressListener(new UtteranceProgressListener() {
            @Override
            public void onStart(String utteranceId) {
                Log.d(TAG, "Started speech synthesis for failure message for utterance id: " + utteranceId);
            }

            @Override
            public void onDone(String utteranceId) {
                String failureFileName = m_cache_path + "/" + utteranceId + ".wav";
                Log.d(TAG, "Got notice that speech synthesis for failure message for utterance id " + utteranceId +
                        " finished, adding this filename to failureFileNames: " + failureFileName);
                m_failureFileNames.add(failureFileName);

                String seqNumString = utteranceId.substring(utteranceId.indexOf("|") + 1, utteranceId.lastIndexOf("|"));
                Log.d(TAG, "Sequence number of this failure file: " + seqNumString);
                long seqNum = Long.parseLong(seqNumString);
                String userId = utteranceId.substring(0, utteranceId.indexOf("|"));
                Log.d(TAG, "User id of this failure file: " + userId);

                if (!m_userAudioBuffers.containsKey(userId)) {
                    Log.d(TAG, "Added new user buffered audio manager for userId: " + userId);
                    m_userAudioBuffers.put(userId,
                            new UserAudioClips(
                                    new UserBufferedAudioManager(seqNum - 1, userId, MISSING_SYNC_SEQ_WAIT_TIME),
                                    new UserBufferedAudioManager(seqNum - 1, userId, MISSING_AUDIO_SEQ_WAIT_TIME)
                            )
                    );
                }

                PrioritizedByteArray failureAudio = new PrioritizedByteArray(0, new byte[]{});
                try {
                    failureAudio = new PrioritizedByteArray(seqNum * 2,
                            readFileToByteArray(new File(failureFileName)));
                } catch (IOException e) {
                    Log.e(TAG, "FAILED TO GET BYTES OF RETRIEVAL FAILURE AUDIO FILE: " + utteranceId + ".wav");
                    e.printStackTrace();
                }


                UserBufferedAudioManager syncAudioClips = m_userAudioBuffers.get(userId).syncAudioClips;
                UserBufferedAudioManager voiceAndRetrievalFailureAudioClips = m_userAudioBuffers.get(userId).voiceAndRetrievalFailureAudioClips;

                /*
                while (syncAudioClips.getLastConsecutiveSeqNum()
                        < seqNum) {
                    Log.d(TAG, "Waiting for sync audio clip to be played before entering " +
                            " this retrieval failure clip to be played...");
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                */

                voiceAndRetrievalFailureAudioClips.insertIntoQueue(new PrioritizedByteArray((seqNum * 2) - 1, new byte[] {}));
                voiceAndRetrievalFailureAudioClips.insertIntoQueue(failureAudio);

                m_currently_generating_retrieval_failure_audio = false;
                cancelRetrievalGenerationFailureTimer();
            }

            @Override
            public void onError(String utteranceId) {
                Log.d(TAG, "Error during speech synthesis for failure message for utterance id: " + utteranceId);

                m_currently_generating_retrieval_failure_audio = false;
                cancelRetrievalGenerationFailureTimer();
            }
        });

        m_sync_tts = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int i) {
                if (i != TextToSpeech.ERROR) {
                    Log.d(TAG, "Text to speech object for sync notification was successfully initialized.");
                    m_sync_tts.setLanguage(Locale.US);
                } else {
                    Log.e(TAG, "TEXT TO SPEECH OBJECT FOR SYNC NOTIFICATION FAILED TO INITIALIZE");
                }
            }
        });

        m_sync_tts.setOnUtteranceProgressListener(new UtteranceProgressListener() {
            @Override
            public void onStart(String utteranceId) {
                Log.d(TAG, "sync text to speech object started for: " + utteranceId);
            }

            @Override
            public void onDone(String utteranceId) {
                String syncNotifFileName = utteranceId;
                Log.d(TAG, "Got notice that speech synthesis for sync notification for utterance id " + utteranceId +
                        " finished, adding this filename to sync notifications " + syncNotifFileName);
                m_syncAudioNotificationNames.add(syncNotifFileName);

                m_currently_generating_sync_audio = false;
                cancelSyncAudioGenerationTimer();
            }

            @Override
            public void onError(String utteranceId) {
                Log.d(TAG, "Error during speech synthesis for sync notification for utterance id: " + utteranceId);

                m_currently_generating_sync_audio = false;
                cancelSyncAudioGenerationTimer();
            }

            @Override
            public void onBeginSynthesis(String utteranceId, int sampleRateInHz, int audioFormat, int channelCount) {

                Log.d(TAG, "Started speech synthesis for sync notification for utterance id: " + utteranceId);

                super.onBeginSynthesis(utteranceId, sampleRateInHz, audioFormat, channelCount);
            }
        });

        m_name_tts = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int i) {
                if (i != TextToSpeech.ERROR) {
                    Log.d(TAG, "Text to speech object for generating name audio " +
                            " was successfully initialized.");
                    m_name_tts.setLanguage(Locale.US);
                } else {
                    Log.e(TAG, "TEXT TO SPEECH OBJECT FOR GENERATING NAME AUDIO " +
                            " FAILED TO INITIALIZE");
                }
            }
        });

        m_name_tts.setOnUtteranceProgressListener(new UtteranceProgressListener() {
            @Override
            public void onStart(String utteranceId) {
                Log.d(TAG, "name text to speech object started for: " + utteranceId);
            }

            @Override
            public void onDone(String utteranceId) {
                String nameAudioFileName = m_cache_path + "/" + utteranceId + ".wav";
                String userId = utteranceId.substring(0, utteranceId.indexOf('|'));
                Log.d(TAG, "Got notice that speech synthesis for sync notification for utterance id " + utteranceId +
                        " finished, adding user id " + userId + "'s name audio bytes to hashmap " + nameAudioFileName);

                byte[] nameAudioBytes = null;
                try {
                    nameAudioBytes = readFileToByteArray(new File(nameAudioFileName));
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if (nameAudioBytes == null) {
                    Log.e(TAG, "Problem getting name audio bytes for name audio file: " + nameAudioFileName);
                    return;
                }

                Log.d(TAG, "Inserting this user id into name audio bytes: " + userId);

                Log.d(TAG, "String for user id in failure generation: " + "|" + userId + "|" +
                        "\n" + "Length of userId in failure generation: " + userId.length());

                m_nameAudioBytes.put(userId, nameAudioBytes);

                m_currently_generating_name_audio = false;
                cancelNameGenerationTimer();
            }

            @Override
            public void onError(String utteranceId) {
                Log.d(TAG, "Error during speech synthesis for name audio for utterance id: " + utteranceId);

                m_currently_generating_name_audio = false;
                cancelNameGenerationTimer();

                String name = utteranceId.substring(utteranceId.indexOf('|') + 1);
                String userId = utteranceId.substring(0, utteranceId.indexOf('|'));
                Log.d(TAG, "Reattempting to generate name audio for " + name + ", user id " + userId);
                String nameFileName = m_cache_path + "/" + utteranceId + ".wav";
                File nameAudioFile = new File(nameFileName);
                Bundle synthesisBundle = new Bundle();
                synthesisBundle.putString(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, utteranceId);

                Log.d(TAG, "About to call synthesize for a name audio");
                int r = m_name_tts.synthesizeToFile(name, synthesisBundle,
                        nameAudioFile, utteranceId);

                startNameGenerationTimer();
            }

            @Override
            public void onBeginSynthesis(String utteranceId, int sampleRateInHz, int audioFormat, int channelCount) {

                Log.d(TAG, "Started speech synthesis for sync notification for utterance id: " + utteranceId);

                super.onBeginSynthesis(utteranceId, sampleRateInHz, audioFormat, channelCount);
            }
        });

        File testNfdFile = new File(m_nfdFailureFileName);
        File testWifiFile = new File(m_wifiFailureFileName);
        File testNfdRecoveryFile = new File(m_nfdRecoveryFileName);
        File testNfdFailureRejectionFile = new File(m_nfdFailureRejectionFileName);
        if (!testNfdFile.exists() || !testWifiFile.exists() || !testNfdRecoveryFile.exists() || !testNfdFailureRejectionFile.exists()) {
            generateStatusClips(m_nfdFailureFileName, m_wifiFailureFileName, m_nfdRecoveryFileName, m_nfdFailureRejectionFileName);
        } else {
            try {
                m_nfdFailureMessageBytes = readFileToByteArray(testNfdFile);
            } catch (IOException e) {
                e.printStackTrace();
            }

            Log.d(TAG, "Size of nfd failure message bytes: " + Integer.toString(m_nfdFailureMessageBytes.length));

            try {
                m_nfdRecoveryMessageBytes = readFileToByteArray(testNfdRecoveryFile);
            } catch (IOException e) {
                e.printStackTrace();
            }

            Log.d(TAG, "Size of nfd recovery message bytes: " + Integer.toString(m_nfdRecoveryMessageBytes.length));

            try {
                m_nfdFailureRejectionMessageBytes = readFileToByteArray(testNfdFailureRejectionFile);
            } catch (IOException e) {
                e.printStackTrace();
            }

            Log.d(TAG, "Size of nfd recovery message bytes: " + Integer.toString(m_nfdFailureRejectionMessageBytes.length));
        }

        m_failureFileNames = new LinkedBlockingQueue<>();
        m_syncAudioNotificationNames = new LinkedBlockingQueue<>();

        m_failureGenerationInfos = new LinkedBlockingQueue<>();
        m_syncAudioGenerationInfos = new LinkedBlockingQueue<>();

        m_namesToGenerate = new LinkedBlockingQueue<>();
        m_nameAudioBytes = new ConcurrentHashMap<>();

        failureMessagePublisherThread.start();
        syncAudioNotificationPublisherThread.start();

        failureMessageGeneratorThread.start();
        syncAudioNotificationGeneratorThread.start();

        nameAudioGeneratorThread.start();

        wifiStatusCheckerThread.start();

        loggerThread.start();
        m_logMessages = new LinkedBlockingQueue<>();

        genTone();
    }

    @Override
    public void onStop() {
        super.onStop();

        if (m_audioManager != null) {
            m_audioManager.close();
        }
    }

    private final ServiceConnection mRadioServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.d(TAG, "onServiceConnected for nfdServiceConnection got called.");
            m_radio_service = ((NDNRadioService.LocalBinder) service).getService();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            m_radio_service = null;

            Log.d(TAG, "The NDN radio service was disconnected.");
        }
    };

    @Override
    protected void onDestroy() {

        LocalBroadcastManager.getInstance(this).unregisterReceiver(ndnRadioBroadcastReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receivedAudioListener);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(pttPressListener);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(localAudioManagerListener);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(ndnRadioBroadcastReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(userBufferedAudioManagersListener);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(failureListener);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(bufferedAudioPlayerAndErrorPlayerListener);

        if (m_sync_tts != null)
            m_sync_tts.shutdown();
        if (m_message_failure_tts != null)
            m_message_failure_tts.shutdown();
        if (m_temp_tts != null)
            m_temp_tts.shutdown();
        if (m_name_tts != null)
            m_name_tts.shutdown();

        if (mRadioServiceConnection != null) {
            try {
                unbindService(mRadioServiceConnection);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (m_audioManager != null) {
            m_audioManager.close();
        }

        File lastRecordedAudioFile = new File(m_cache_path + "/" + "final.wav");
        Log.d(TAG, "Attempting to delete file with this path: " + lastRecordedAudioFile.getAbsolutePath());
        lastRecordedAudioFile.delete();

        File lastReceivedAudioFile = new File(m_cache_path + "/" + "last.wav");
        Log.d(TAG, "Attempting to delete file with this path: " + lastRecordedAudioFile.getAbsolutePath());
        lastReceivedAudioFile.delete();

        super.onDestroy();
    }

    private final BroadcastReceiver ndnRadioBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction().toString();
            if (action.equals(NDNRadioService.ACTION_RECEIVED)) {
                m_action_text.setText("We received some data from sync.");
                //byte[] receivedData = intent.getByteArrayExtra(NDNRadioService.EXTRA_AUDIO);
            }
        }
    };

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_RECORD_AUDIO_PERMISSION:
                permissionToRecordAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                break;
        }
        if (!permissionToRecordAccepted) finish();

    }

    // starts the radio service
    private void startRadioService() {
        Intent radioIntent = new Intent(MainActivity.this, NDNRadioService.class);

        String[] radioInfo = new String[5];
        radioInfo[NDNRadioService.CHANNEL] = m_channel;
        radioInfo[NDNRadioService.NAME] = m_name;
        radioInfo[NDNRadioService.SEGMENT_INTEREST_MAX_REATTEMPTS] = m_segment_interest_max_reattempts;
        radioInfo[NDNRadioService.SEGMENT_INTEREST_LIFETIME] = m_segment_interest_lifetime;
        radioInfo[NDNRadioService.AP_IP_ADDRESS] = m_ap_ip_address;

        m_name_display.setText(m_name);
        m_segment_interest_max_reattempts_display.setText(m_segment_interest_max_reattempts);
        m_segment_interest_lifetime_display.setText(m_segment_interest_lifetime);

        radioIntent.putExtra(NDNRadioService.EXTRA_RADIO_INFO, radioInfo);
        boolean success = bindService(radioIntent, mRadioServiceConnection, BIND_AUTO_CREATE);
        if (success) {
            Log.d(TAG, "bindService for ndn radio service was successful");
        } else {
            Log.d(TAG, "bindService for ndn radio service was not successful");
            startRadioService();
        }
    }

    public static MainActivity getInstance() {
        return mainActivity;
    }


    private boolean channelAndNameAreSelected() {
        return (m_channel != null && m_name != null);
    }

    private void clearChannelInfo() {
        m_channel = null;
    }

    private void launchLoginActivity() {
        clearChannelInfo();
        startActivityForResult(new Intent(this, LoginActivity.class), 0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != RESULT_OK) {
            Log.d(TAG, "SOMETHING WENT WRONG GETTING CHANNEL AND NAME FROM LOGIN ACTIVITY");
            return;
        }

        String[] radioInfo = data.getStringArrayExtra(NDNRadioService.EXTRA_RADIO_INFO);
        String channel = radioInfo[NDNRadioService.CHANNEL];
        String name = radioInfo[NDNRadioService.NAME];
        String segmentInterestLifetime = radioInfo[NDNRadioService.SEGMENT_INTEREST_LIFETIME];
        String segmentInterestMaxReattempts = radioInfo[NDNRadioService.SEGMENT_INTEREST_MAX_REATTEMPTS];
        String apIpAddress = radioInfo[NDNRadioService.AP_IP_ADDRESS];

        setChannelInfo(channel);
        setNameInfo(name);
        setSegmentInterestLifetime(segmentInterestLifetime);
        setSegmentInterestMaxReattempts(segmentInterestMaxReattempts);
        setApIpAddress(apIpAddress);

        startRadioService();
    }

    private void setChannelInfo(String channel) {
        m_channel = channel;
    }

    private void setNameInfo(String name) {
        m_name = name;
    }

    private void setSegmentInterestLifetime(String segmentInterestLifetime)
    { m_segment_interest_lifetime = segmentInterestLifetime; }

    private void setSegmentInterestMaxReattempts(String segmentInterestMaxReattempts)
    { m_segment_interest_max_reattempts = segmentInterestMaxReattempts; }

    private void setApIpAddress(String apIpAddress)
    { m_ap_ip_address = apIpAddress; }

    Thread failureMessagePublisherThread = new Thread(new Runnable() {

        public void run() {
            while (true) {
                if (m_failureFileNames.size() != 0) {
                    String currentPath = m_failureFileNames.poll();

                    File failureFile = new File(currentPath);

                    byte[] audioBytes;
                    try {
                        audioBytes = readFileToByteArray(failureFile);
                    } catch (IOException e) {
                        e.printStackTrace();
                        continue;
                    }

                    Log.d(TAG, "Length of audiobytes for failure: " + audioBytes.length);

                    m_bufferedAudioPlayer.addAudioToAudioClipsBuffer(audioBytes);

                    failureFile.delete();
                }

                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

    });

    Thread syncAudioNotificationPublisherThread = new Thread(new Runnable() {

        public void run() {
            while (true) {
                if (m_syncAudioNotificationNames.size() != 0) {
                    String utteranceId = m_syncAudioNotificationNames.poll();
                    String currentPath = m_cache_path + "/" + utteranceId + ".wav";

                    String userId = utteranceId.substring(0, utteranceId.indexOf('|'));
                    long seqNum =
                            Long.parseLong(
                                    utteranceId.substring(
                                            utteranceId.indexOf('|') + 1,
                                            utteranceId.lastIndexOf('|')
                                    )
                            );
                    Log.d(TAG, "userId gotten from utterance id in sync audio notification publisher thread: " + userId);
                    Log.d(TAG, "seqNum gotten from utterance id in sync audio notification publisher thread:" + Long.toString(seqNum));

                    File syncAudioFile = new File(currentPath);

                    byte[] bytesOfAudio;
                    try {
                        bytesOfAudio = readFileToByteArray(syncAudioFile);
                    } catch (IOException e) {
                        e.printStackTrace();
                        continue;
                    }

                    Log.d(TAG, "Length of audiobytes for sync notification: " + bytesOfAudio.length);

                    if (!m_userAudioBuffers.containsKey(userId)) {
                        Log.d(TAG, "Added new user buffered audio manager for userId: " + userId);
                        m_userAudioBuffers.put(userId,
                                new UserAudioClips(
                                        new UserBufferedAudioManager(seqNum - 1, userId, MISSING_SYNC_SEQ_WAIT_TIME),
                                        new UserBufferedAudioManager(seqNum - 1, userId, MISSING_AUDIO_SEQ_WAIT_TIME))
                        );
                    }

                    PrioritizedByteArray temp = new PrioritizedByteArray(seqNum, bytesOfAudio);

                    m_userAudioBuffers.get(userId).syncAudioClips.insertIntoQueue(temp);

                    syncAudioFile.delete();
                }

                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

    });

    Thread syncAudioNotificationGeneratorThread = new Thread(new Runnable() {

        public void run() {
            while (true) {
                if (m_syncAudioGenerationInfos.size() != 0) {

                    while (m_currently_generating_sync_audio) {
                        Log.d(TAG, "Waiting for last sync audio generation to finish...");
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }

                    GeneratedMessageInfo currentInfo = m_syncAudioGenerationInfos.poll();

                    String userId = currentInfo.userId;
                    String userName = currentInfo.userName;
                    long seqNum = currentInfo.seqNum;

                    String syncNotifMessage = "Message " + seqNum + " from " + userName;

                    String syncNotifUtteranceId = userId + "|" + seqNum + "|" + "syncnotif";
                    String syncNotifFileName = m_cache_path + "/" + syncNotifUtteranceId + ".wav";
                    File syncNotifAudioFile = new File(syncNotifFileName);
                    Bundle synthesisBundle = new Bundle();
                    synthesisBundle.putString(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, syncNotifUtteranceId);

                    int r = m_sync_tts.synthesizeToFile(syncNotifMessage, synthesisBundle,
                            syncNotifAudioFile, syncNotifUtteranceId);

                    m_currently_generating_sync_audio = true;

                    startSyncAudioGenerationTimer();
                }

                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

    });

    Thread failureMessageGeneratorThread = new Thread(new Runnable() {

        public void run() {
            while (true) {
                if (m_failureGenerationInfos.size() != 0) {

                    while (m_currently_generating_retrieval_failure_audio) {
                        Log.d(TAG, "Waiting for last failure retrieval audio generation to finish...");
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }

                    GeneratedMessageInfo currentInfo = m_failureGenerationInfos.poll();

                    String userId = currentInfo.userId;
                    String userName = currentInfo.userName;
                    long seqNum = currentInfo.seqNum;
                    String errorCode = currentInfo.errorCode;

                    if (!m_userAudioBuffers.containsKey(userId)) {
                        Log.d(TAG, "Added new user buffered audio manager for userId: " + userId);
                        m_userAudioBuffers.put(userId,
                                new UserAudioClips(
                                        new UserBufferedAudioManager(seqNum - 1, userId, MISSING_SYNC_SEQ_WAIT_TIME),
                                        new UserBufferedAudioManager(seqNum - 1, userId, MISSING_AUDIO_SEQ_WAIT_TIME))
                        );
                    }

                    UserBufferedAudioManager u = m_userAudioBuffers.get(userId).voiceAndRetrievalFailureAudioClips;

                    if (u == null) {
                        Log.e(TAG, "Did not find a user buffered audio manager for " + userId);
                        return;
                    }

                    UserBufferedAudioManager syncAudioClips = m_userAudioBuffers.get(userId).syncAudioClips;
                    UserBufferedAudioManager voiceAndRetrievalFailureAudioClips = m_userAudioBuffers.get(userId).voiceAndRetrievalFailureAudioClips;

                    /*
                    while (syncAudioClips.getLastConsecutiveSeqNum()
                            < seqNum) {
                        Log.d(TAG, "Waiting for sync audio clip to be played before entering " +
                                " this retrieval failure clip to be played...");
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    */

                    // this is code that should be uncommented if you just want to play beep for failures
                    //voiceAndRetrievalFailureAudioClips.insertIntoQueue(new PrioritizedByteArray((seqNum * 2) - 1, new byte[] {}));
                    //voiceAndRetrievalFailureAudioClips.insertIntoQueue(new PrioritizedByteArray(seqNum * 2, beepSnd));

                    // Davide and Junxiao do not want failure messages to be played, so will not generate failure message

                    //u.notifyFailedSeqNumber(new PrioritizedSeqNum(seqNum));

                    String failureMessage = "";
                    if (errorCode.equals(SegmentFetcherCustom.ErrorCode.INTEREST_TIMEOUT.toString()))
                        failureMessage = userName + " failure timeout " + seqNum;
                    else if (errorCode.equals(SegmentFetcherCustom.ErrorCode.SEGMENT_VERIFICATION_FAILED.toString()))
                        failureMessage = userName + " failure verify " + seqNum;
                    else if (errorCode.equals(SegmentFetcherCustom.ErrorCode.DATA_HAS_NO_SEGMENT.toString()))
                        failureMessage = userName + " failure no segment " + seqNum;
                    else if (errorCode.equals(SegmentFetcherCustom.ErrorCode.IO_ERROR.toString()))
                        failureMessage = userName + " failure io error " + seqNum;

                    Log.d(TAG, failureMessage);

                    String utteranceId = userId + "|" + seqNum + "|" + "failure";
                    String failureFileName = m_cache_path + "/" + utteranceId + ".wav";
                    File failureAudioFile = new File(failureFileName);
                    Bundle synthesisBundle = new Bundle();
                    synthesisBundle.putString(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, utteranceId);

                    Log.d(TAG, "About to call synthesize for a failure retrieval audio");
                    int r = m_message_failure_tts.synthesizeToFile(failureMessage, synthesisBundle,
                            failureAudioFile, utteranceId);

                    m_currently_generating_retrieval_failure_audio = true;

                    startRetrievalFailureGenerationTimer();

                }

                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

    });

    Thread nameAudioGeneratorThread = new Thread(new Runnable() {

        public void run() {
            while (true) {
                if (m_namesToGenerate.size() != 0) {

                    UserIdAndName info = m_namesToGenerate.poll();
                    String userId = info.userId;
                    String name = info.name;

                    String utteranceId = userId + "|" + name;
                    String nameFileName = m_cache_path + "/" + utteranceId + ".wav";
                    File nameAudioFile = new File(nameFileName);
                    Bundle synthesisBundle = new Bundle();
                    synthesisBundle.putString(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, utteranceId);

                    Log.d(TAG, "About to call synthesize for a name audio");
                    int r = m_name_tts.synthesizeToFile(name, synthesisBundle,
                            nameAudioFile, utteranceId);

                    m_currently_generating_name_audio = true;

                    startNameGenerationTimer();
                }

                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

    });

    Thread loggerThread = new Thread(new Runnable() {

        public void run() {
            while (true) {
                if (m_logMessages.size() != 0) {

                    try {
                        writeToLog(m_logMessages.poll());
                    } catch (IOException e) {
                        Log.e(TAG, "Problem writing to log");
                        e.printStackTrace();
                    }

                }

                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

    });

    private void writeToLog(String message) throws IOException {
        final String entry = "---" + "\n" + message + "\n";
        FileOutputStream fOut = new FileOutputStream
                (new File(absolutePath + "/" + m_startUpTime + "-" + "log.txt"),
                        true); // true will be same as Context.MODE_APPEND
        OutputStreamWriter osw = new OutputStreamWriter(fOut);
        osw.write(entry);
        osw.flush();
        osw.close();
    }

    Thread wifiStatusCheckerThread = new Thread(new Runnable() {

        public void run() {
            while (true) {

                connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

                if (mWifi.isConnected() && !lastWifiConnectionState) {
                    Log.d(TAG, "Wifi got connected.");
                    lastWifiConnectionState = true;
                    m_logMessages.add(Long.toString(System.currentTimeMillis()) + "_" + "WIFI" + "_" + "ON");
                }
                else if (!mWifi.isConnected() && lastWifiConnectionState) {
                    Log.d(TAG, "Wifi got disconnected.");
                    lastWifiConnectionState = false;
                    m_logMessages.add(Long.toString(System.currentTimeMillis()) + "_" + "WIFI" + "_" + "OFF");
                }

                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

    });

    public void sendLastAudio() {
        File recording = new File(m_cache_path + "/" + "final.wav");
        byte[] recordingBytes = new byte[0];

        Log.d(TAG, "recording bytes length before reading: " + recordingBytes.length);
        try {
            recordingBytes = readFileToByteArray(recording);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Log.d(TAG, "recording bytes length after reading: " + recordingBytes.length);
        m_last_size_display.setText(Integer.toString(recordingBytes.length));

        Log.d(TAG, "The message was longer than the maximum ndn packet size, performing fragmentation.");
        int numFragments = getNumberOfFragments(recordingBytes.length);
        Log.d(TAG, "Number of fragments for audio recording: " + Integer.toString(numFragments));

        ArrayList<byte[]> tempArrList = createFragments(recordingBytes);

        m_radio_service.sendMessage(tempArrList);

        m_action_text.setText("Sent chronosync data with segments number (extra 1 for" +
                " segments number in beginning of actual fragments arraylist): " + tempArrList.size());

    }

    private void generateStatusClips(final String m_nfdFailureFileName, final String m_wifiFailureFileName,
                                     final String m_nfdRecoveryFileName, final String m_nfdFailureRejectionFileName) {
        final String nfdFailureMessage = "Warning: NFD failed.";
        final String wifiFailureMessage = "Warning: Wifi failed.";
        final String nfdRecoveryMessage = "NFD recovered.";
        final String nfdFailureRejectionMessage = "Did not record or send message because NFD has failed.";
        final File nfdFailureAudioFile = new File(m_nfdFailureFileName);
        final File wifiFailureAudioFile = new File(m_wifiFailureFileName);
        final File nfdRecoveryAudioFile = new File(m_nfdRecoveryFileName);
        final File nfdFailureRejectionAudioFile = new File(m_nfdFailureRejectionFileName);
        final Bundle nfdSynthesisBundle = new Bundle();
        final Bundle wifiSynthesisBundle = new Bundle();
        final Bundle nfdRecoverySynthesisBundle = new Bundle();
        final Bundle nfdFailureRejectionSynthesisBundle = new Bundle();
        nfdSynthesisBundle.putString(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, m_nfdFailureUtteranceId);
        wifiSynthesisBundle.putString(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, m_wifiFailureUtteranceId);
        nfdRecoverySynthesisBundle.putString(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, m_nfdRecoveryUtteranceId);
        nfdFailureRejectionSynthesisBundle.putString(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, m_nfdFailureRejectionUtteranceId);

        m_temp_tts = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int i) {
                if (i != TextToSpeech.ERROR) {
                    Log.d(TAG, "Text to speech object for generating NFD and wifi failure messages" +
                            " was successfully initialized.");
                    m_temp_tts.setLanguage(Locale.US);

                    Log.d(TAG, "nfd failure file name: " + m_nfdFailureFileName);
                    m_nfdFailureFile = new File(m_nfdFailureFileName);

                    m_temp_tts.synthesizeToFile(nfdFailureMessage, nfdSynthesisBundle,
                            nfdFailureAudioFile, m_nfdFailureUtteranceId);
                } else {
                    Log.e(TAG, "TEXT TO SPEECH OBJECT FOR GENERATING NFD AND WIFI FAILURE MESSAGES" +
                            " FAILED TO INITIALIZE");
                }
            }
        });

        m_temp_tts.setOnUtteranceProgressListener(new UtteranceProgressListener() {
            @Override
            public void onStart(String s) {

            }

            @Override
            public void onDone(String s) {
                Log.d(TAG, "Successfully synthesized audio for utterance id " + s);
                if (s.equals(m_nfdFailureUtteranceId)) {

                    m_nfdRecoveryFile = new File(m_nfdRecoveryFileName);

                    m_temp_tts.synthesizeToFile(nfdRecoveryMessage, nfdRecoverySynthesisBundle,
                            nfdRecoveryAudioFile, m_nfdRecoveryUtteranceId);

                    try {
                        m_nfdFailureMessageBytes = readFileToByteArray(m_nfdFailureFile);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    Log.d(TAG, "Size of nfd failure message bytes: " + Integer.toString(m_nfdFailureMessageBytes.length));

                } else if (s.equals(m_nfdRecoveryUtteranceId)) {

                    m_nfdFailureRejectionFile = new File(m_nfdFailureRejectionFileName);

                    m_temp_tts.synthesizeToFile(nfdFailureRejectionMessage, nfdFailureRejectionSynthesisBundle,
                            nfdFailureRejectionAudioFile, m_nfdFailureRejectionUtteranceId);

                    try {
                        m_nfdRecoveryMessageBytes = readFileToByteArray(m_nfdRecoveryFile);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    Log.d(TAG, "Size of nfd recovery message bytes: " + Integer.toString(m_nfdFailureMessageBytes.length));

                } else if (s.equals(m_nfdFailureRejectionUtteranceId)) {

                    m_wifiFailureFile = new File(m_wifiFailureFileName);

                    m_temp_tts.synthesizeToFile(wifiFailureMessage, wifiSynthesisBundle,
                            wifiFailureAudioFile, m_wifiFailureUtteranceId);

                    try {
                        m_nfdFailureRejectionMessageBytes = readFileToByteArray(m_nfdFailureRejectionFile);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    Log.d(TAG, "Size of nfd failure rejection message bytes: " + Integer.toString(m_nfdFailureRejectionMessageBytes.length));

                } else if (s.equals(m_wifiFailureUtteranceId)) {

                    try {
                        m_wifiFailureMessageBytes = readFileToByteArray(m_wifiFailureFile);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onError(String s) {
                Log.e(TAG, "Failure synthesizing audio for utterance id " + s);
            }
        });
    }

    private void startWaitTimer() {
        if (m_timer != null) {
            m_timer.cancel();
            m_timer = null;
            m_timer = new Timer();
        } else {
            m_timer = new Timer();
        }
        m_timer.schedule(new TimerTask() {
            @Override
            public void run() {

                Log.d(TAG, "Button is now pressable again");

                m_button_pressable = true;
            }

            @Override
            public boolean cancel() {

                Log.d(TAG, "button wait timer got cancelled");

                return super.cancel();
            }
        }, M_BUTTON_PRESS_TIME_LIMIT);
    }

    private void cancelWaitTimer() {
        if (m_timer != null) {
            m_timer.cancel();
            m_timer = null;
        }
    }

    private void startSyncAudioGenerationTimer() {
        if (m_sync_audio_generation_limit_timer != null) {
            m_sync_audio_generation_limit_timer.cancel();
            m_sync_audio_generation_limit_timer = null;
            m_sync_audio_generation_limit_timer = new Timer();
        } else {
            m_sync_audio_generation_limit_timer = new Timer();
        }
        m_sync_audio_generation_limit_timer.schedule(new TimerTask() {
            @Override
            public void run() {

                Log.d(TAG, "sync audio generation timer ran out");

                m_sync_tts.stop();
                m_currently_generating_sync_audio = false;
            }

            @Override
            public boolean cancel() {

                Log.d(TAG, "sync audio generation timer got cancelled");

                m_currently_generating_sync_audio = false;

                return super.cancel();
            }
        }, M_SYNC_AUDIO_GENERATION_TIME_LIMIT);
    }

    private void cancelSyncAudioGenerationTimer() {
        if (m_sync_audio_generation_limit_timer != null) {
            m_sync_audio_generation_limit_timer.cancel();
            m_sync_audio_generation_limit_timer = null;
        }
    }

    private void startRetrievalFailureGenerationTimer() {
        if (m_retrieval_failure_audio_generation_limit_timer != null) {
            m_retrieval_failure_audio_generation_limit_timer.cancel();
            m_retrieval_failure_audio_generation_limit_timer = null;
            m_retrieval_failure_audio_generation_limit_timer = new Timer();
        } else {
            m_retrieval_failure_audio_generation_limit_timer = new Timer();
        }
        m_retrieval_failure_audio_generation_limit_timer.schedule(new TimerTask() {
            @Override
            public void run() {

                Log.d(TAG, "retrieval failure audio generation timer ran out");

                m_message_failure_tts.stop();
                m_currently_generating_retrieval_failure_audio = false;

            }

            @Override
            public boolean cancel() {

                Log.d(TAG, "failure generation timer got cancelled");

                m_currently_generating_retrieval_failure_audio = false;

                return super.cancel();
            }
        }, M_RETRIEVAL_FAILURE_AUDIO_GENERATION_TIME_LIMIT);
    }

    private void cancelRetrievalGenerationFailureTimer() {
        if (m_retrieval_failure_audio_generation_limit_timer != null) {
            m_retrieval_failure_audio_generation_limit_timer.cancel();
            m_retrieval_failure_audio_generation_limit_timer = null;
        }
    }

    private void startNameGenerationTimer() {
        if (m_name_audio_generation_limit_timer != null) {
            m_name_audio_generation_limit_timer.cancel();
            m_name_audio_generation_limit_timer = null;
            m_name_audio_generation_limit_timer = new Timer();
        } else {
            m_name_audio_generation_limit_timer = new Timer();
        }
        m_name_audio_generation_limit_timer.schedule(new TimerTask() {
            @Override
            public void run() {

                Log.d(TAG, "retrieval failure audio generation timer ran out");

                m_name_tts.stop();
                m_currently_generating_name_audio = false;

            }

            @Override
            public boolean cancel() {

                Log.d(TAG, "failure generation timer got cancelled");

                m_currently_generating_name_audio = false;

                return super.cancel();
            }
        }, M_NAME_AUDIO_GENERATION_LIMIT_TIMER);
    }

    private void cancelNameGenerationTimer() {
        if (m_name_audio_generation_limit_timer != null) {
            m_name_audio_generation_limit_timer.cancel();
            m_name_audio_generation_limit_timer = null;
        }
    }

    void genTone(){
        // fill out the array
        for (int i = 0; i < numSamples; ++i) {
            sample[i] = Math.sin(2 * Math.PI * i / (sampleRate/freqOfTone));
        }

        // convert to 16 bit pcm sound array
        // assumes the sample buffer is normalised.
        int idx = 0;
        for (final double dVal : sample) {
            // scale to maximum amplitude
            final short val = (short) ((dVal * 32767));
            // in 16 bit wav PCM, first byte is the low order byte
            rawBeepSnd[idx++] = (byte) (val & 0x00ff);
            rawBeepSnd[idx++] = (byte) ((val & 0xff00) >>> 8);

        }

        File rawBeepFile = new File(m_cache_path + "/beep.raw");
        try {
            rawBeepFile.createNewFile();
            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(rawBeepFile));
            bos.write(rawBeepSnd);
            bos.flush();
            bos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            WavToAndFromRawHelpers.rawToWave(rawBeepFile, new File(m_cache_path + "/beep.wav"));
        } catch (IOException e) {
            e.printStackTrace();
        }

       try {
           beepSnd = readFileToByteArray(new File(m_cache_path + "/beep.wav"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void logMessage(String[] notificationInfo, String action) {

        String userId = notificationInfo[NDNRadioService.LOGINFO_USER_ID];
        String seqNum = notificationInfo[NDNRadioService.LOGINFO_SEQ_NUM];
        String userName = notificationInfo[NDNRadioService.LOGINFO_USER_NAME];

        if (action.equals(NDNRadioService.ACTION_GOT_SYNC)) {
            m_logMessages.add(System.currentTimeMillis() + "_" + "GOTSYNC" + "_" + userName + "_" + userId + "_" + seqNum);
        }
        else if (action.equals(NDNRadioService.ACTION_FAILED_FETCH)) {
            m_logMessages.add(System.currentTimeMillis() + "_"  + "FAILEDFETCH"  + "_" + userName + "_" + userId + "_" + seqNum);
        }
        else if (action.equals(NDNRadioService.ACTION_FINISHED_FETCH)) {
            // numSegments is actuall final block id, need to add one to get number of segments
            String numSegments = notificationInfo[NDNRadioService.LOGINFO_NUM_SEGMENTS];
            long actualNumSegments = Long.parseLong(numSegments) + 1;
            m_logMessages.add(System.currentTimeMillis() + "_"  + "FINISHEDFETCH"  + "_" + userName + "_" + userId + "_" + seqNum
            + "_" + Long.toString(actualNumSegments));
        }
        else if (action.equals(NDNRadioService.ACTION_STARTED_FETCH)) {
            m_logMessages.add(System.currentTimeMillis() + "_"  + "STARTEDFETCH"  + "_" + userName + "_" + userId + "_" + seqNum);
        }
        else {
            Log.e(TAG, "log message got passed an unexpected action: " + action);
        }

    }

    private void logPublishMessage(long seqNum) {
        m_logMessages.add(System.currentTimeMillis() + "_" + "PUBLISHEDMESSAGE" + "_" + m_name + "_" +
        m_radio_service.m_uuid + "_" + Long.toString(seqNum));
    }
}