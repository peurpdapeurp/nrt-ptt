package com.example.ezl.ndnradiotest;

import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.PriorityBlockingQueue;

public class UserBufferedAudioManager {

    private final String TAG = "UsrBfrdAudioMngr";

    public static final String
            ACTION_REQUEST_PLAYING = "ACTION_REQUEST_PLAYING";

    public static final String
            EXTRA_AUDIO_BYTES = "EXTRA_AUDIO_BYTES",
            EXTRA_CHRONOSYNC_SEQ_NUM = "EXTRA_CHRONOSYNC_SEQ_NUM";

    private long MISSING_SEQ_WAIT_TIME;

    private PriorityBlockingQueue<PrioritizedByteArray> m_queue;
    private PriorityBlockingQueue<PrioritizedSeqNum> m_failed_nums_queue;
    private long m_last_consecutive_seq_num;
    private long m_next_highest_seq_num;
    private boolean m_sending_audio;
    private String m_user_id;
    private Timer m_timer;

    public UserBufferedAudioManager(long initialSeqNum, String userId, long missingSeqWaitTime) {
        m_queue = new PriorityBlockingQueue<>();
        m_failed_nums_queue = new PriorityBlockingQueue<>();
        m_last_consecutive_seq_num = initialSeqNum;
        m_next_highest_seq_num = initialSeqNum;
        m_user_id = userId;
        MISSING_SEQ_WAIT_TIME = missingSeqWaitTime;
    }

    public void playBufferedAudio() {

        if (m_queue.size() == 0) {
            Log.d(TAG, "Finished sending all consecutively buffered audio, not setting wait timer.");
            Log.d(TAG, "Current last value: " + m_last_consecutive_seq_num);
            Log.d(TAG, "Current next highest value: " + m_next_highest_seq_num);
            m_sending_audio = false;
            return;
        }

        PrioritizedByteArray currentByteArrayToPlay = m_queue.peek();

        long curPriority = currentByteArrayToPlay.priority;

        if (curPriority <= m_last_consecutive_seq_num) {
            Log.d(TAG, "Inside of play buffered audio, got data with seqNum already seen, weird, popping");
            m_queue.poll();

            playBufferedAudio();

            return;
        }
        else if (curPriority == m_last_consecutive_seq_num + 1) {

            Log.d(TAG, "Inside of play buffered audio, found next consecutive seq nummed audio to play," +
                    " seq num: " + curPriority);

            if (m_next_highest_seq_num == m_last_consecutive_seq_num)
                m_next_highest_seq_num = curPriority;
            m_last_consecutive_seq_num = curPriority;

            Intent requestPlayIntent = new Intent(ACTION_REQUEST_PLAYING);

            requestPlayIntent.putExtra(EXTRA_AUDIO_BYTES, currentByteArrayToPlay.array);

            Log.d(TAG, "SENDING BROADCAST TO PLAY SEQ NUM " + curPriority + " FOR USER " + m_user_id + "\n" +
            "Length of bytes in this audio message: " + currentByteArrayToPlay.array.length);
            LocalBroadcastManager.getInstance(MainActivity.getInstance()).sendBroadcast(requestPlayIntent);

            m_queue.poll();

            playBufferedAudio();

        }
        else if (curPriority > m_last_consecutive_seq_num + 1) {

            Log.d(TAG, "Inside of play buffered audio, found a sequence number that skipped some, " +
                    + curPriority + ", stopping the playing here.");

            if (
                    (curPriority > m_last_consecutive_seq_num &&
                            m_next_highest_seq_num > m_last_consecutive_seq_num &&
                            curPriority < m_next_highest_seq_num)
                            ||
                            (m_last_consecutive_seq_num == m_next_highest_seq_num &&
                                    curPriority > m_last_consecutive_seq_num)
                    )
                m_next_highest_seq_num = curPriority;

            Log.d(TAG, "Next highest sequence number above last consecutive sequence number: " +
                m_next_highest_seq_num);

            m_sending_audio = false;

            startWaitTimer();
        }

    }

    public void insertIntoQueue(PrioritizedByteArray e) {

        Log.d(TAG, "Insert into queue was called for user buffered audio manager of user id " + m_user_id + ", seq num " + e.priority
        + "\n" + "Length of this byte array: " + e.array.length);

        if (e.priority <= m_last_consecutive_seq_num) {
            Log.d(TAG, "Got data with seqNum behind last consecutive, ignoring it. " +
                    "(seq num " + e.priority + ", current consecutive " + m_last_consecutive_seq_num + ")," +
                    " (user id: " + m_user_id);
        }
        else if (e.priority == m_last_consecutive_seq_num + 1) {

            Log.d(TAG, "Got the next consecutive chronosync sequence number we should play, " +
                    e.priority + ", putting it in queue and playing it now.");
            m_queue.add(e);

            cancelWaitTimer();

            if (!m_sending_audio) {
                m_sending_audio = true;
                playBufferedAudio();
            }
        }

        if (e.priority > m_last_consecutive_seq_num + 1) {

            Log.d(TAG, "Got a chronosync sequence number (" + e.priority + ") that skipped some sequence numbers, " +
                    "putting it in queue to be played later");

            if (
                    (e.priority > m_last_consecutive_seq_num &&
                        m_next_highest_seq_num > m_last_consecutive_seq_num &&
                        e.priority < m_next_highest_seq_num)
                                        ||
                    (m_last_consecutive_seq_num == m_next_highest_seq_num &&
                        e.priority > m_last_consecutive_seq_num)
                    )
                m_next_highest_seq_num = e.priority;

            m_queue.add(e);

            startWaitTimer();
        }
    }

    private void startWaitTimer() {
        if (m_timer != null) {
            m_timer.cancel();
            m_timer = null;
            m_timer = new Timer();
        }
        else {
            m_timer = new Timer();
        }
        m_timer.schedule(new TimerTask() {
            @Override
            public void run() {
                Log.d(TAG, "Skip missing seq numbers event got triggered, meaning timer ran out.");
                Log.d(TAG, "Current value of last: " + m_last_consecutive_seq_num);
                Log.d(TAG, "Current value of next: " + m_next_highest_seq_num);
                m_last_consecutive_seq_num = m_next_highest_seq_num - 1;
                if (!m_sending_audio) {

                    Log.d(TAG, "Starting a new play buffered audio.");
                    Log.d(TAG, "Current value of last: " + m_last_consecutive_seq_num);
                    Log.d(TAG, "Current value of next: " + m_next_highest_seq_num);

                    playBufferedAudio();
                }
                else {
                    Log.d(TAG, "m_sending_audio was false when we tried to skip missing seq nums");
                }
            }

            @Override
            public boolean cancel() {

                Log.d(TAG, "skip missing seq nums was cancelled because we got another consecutive seq");

                return super.cancel();
            }
        }, MISSING_SEQ_WAIT_TIME);
    }

    private void cancelWaitTimer() {
        if (m_timer != null) {
            m_timer.cancel();
            m_timer = null;
        }
    }

/*
    public void notifyFailedSeqNumber(PrioritizedSeqNum seqNum) {
        cancelWaitTimer();

        Log.d(TAG, "Adding seqNum " + seqNum.seqNum + " into failed nums queue.");
        m_failed_nums_queue.add(seqNum);

        while(m_failed_nums_queue.size() != 0) {
            PrioritizedSeqNum lowestFailedSeqNum = m_failed_nums_queue.peek();
            long cur = lowestFailedSeqNum.seqNum;
            Log.d(TAG, "Current head of failed nums queue: " + cur);

            if (cur <= m_last_consecutive_seq_num) {
                Log.d(TAG, "Found a failed seqnum " + cur + " that was less than or equal to last consecutive seq num " +
                m_last_consecutive_seq_num + ", popping and ignoring it");
                m_failed_nums_queue.poll();
            }
            else if (cur == m_last_consecutive_seq_num + 1) {
                Log.d(TAG, "Found a failed seqnum " + cur + " that was one more than last consecutive seq num");
                m_failed_nums_queue.poll();
                m_last_consecutive_seq_num++;
            }
            else if (cur > m_last_consecutive_seq_num + 1) {
                Log.d(TAG, "Found a failed seqnum " + cur + " that was more than one past the last consecutive seq num");
                break;
            }
        }

        playBufferedAudio();
    }
*/

    public static IntentFilter getIntentFilter() {
        IntentFilter filter = new IntentFilter();

        filter.addAction(ACTION_REQUEST_PLAYING);

        return filter;
    }

    public long getLastConsecutiveSeqNum() {
        return new Long(m_last_consecutive_seq_num).longValue();
    }

}
