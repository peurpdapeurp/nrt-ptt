package com.example.ezl.ndnradiotest;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.text.format.Time;
import android.util.Log;

import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.FileChannel;

public class WavToAndFromRawHelpers {

    private static final String TAG = "wavToRawHelper";
    public static int SAMPLE_RATE = 22050;

    /* Converting RAW format To WAV Format*/
    public static void rawToWave(final File rawFile, final File waveFile) throws IOException {

        byte[] rawData = new byte[(int) rawFile.length()];
        Log.d(TAG, "Length of raw file: " + rawFile.length());
        DataInputStream input = null;
        try {
            input = new DataInputStream(new FileInputStream(rawFile));
            input.read(rawData);
        } finally {
            if (input != null) {
                input.close();
            }
        }
        DataOutputStream output = null;
        try {
            output = new DataOutputStream(new FileOutputStream(waveFile));
            // WAVE header
            writeString(output, "RIFF"); // chunk id
            writeInt(output, 36 + rawData.length); // chunk size
            writeString(output, "WAVE"); // format
            writeString(output, "fmt "); // subchunk 1 id
            writeInt(output, 16); // subchunk 1 size
            writeShort(output, (short) 1); // audio format (1 = PCM)
            writeShort(output, (short) 1); // number of channels
            writeInt(output, SAMPLE_RATE); // sample rate
            writeInt(output, SAMPLE_RATE * 2); // byte rate
            writeShort(output, (short) 2); // block align
            writeShort(output, (short) 16); // bits per sample
            writeString(output, "data"); // subchunk 2 id
            writeInt(output, rawData.length); // subchunk 2 size
            // Audio data (conversion big endian -> little endian)
            short[] shorts = new short[rawData.length / 2];
            ByteBuffer.wrap(rawData).order(ByteOrder.LITTLE_ENDIAN).asShortBuffer().get(shorts);
            ByteBuffer bytes = ByteBuffer.allocate(shorts.length * 2);
            for (short s : shorts) {
                bytes.putShort(s);
            }
            output.write(bytes.array());
        } finally {
            if (output != null) {
                output.close();
                //rawFile.delete();
            }
        }


    }

    public static void waveToRaw(final File wavFile, final File rawFile) throws IOException {

        byte[] wavData = new byte[(int) wavFile.length()];
        Log.d(TAG, "Length of wav's actual audio bytes: " + (wavFile.length()));
        DataInputStream input = null;
        try {
            input = new DataInputStream(new FileInputStream(wavFile));
            input.read(wavData);
        } finally {
            if (input != null) {
                input.close();
            }
        }
        byte[] actualWavAudio = new byte[(int) wavFile.length() - 44];
        System.arraycopy(wavData, 44, actualWavAudio, 0, (int) (wavFile.length() - 44));
        DataOutputStream output = null;
        try {
            output = new DataOutputStream(new FileOutputStream(rawFile));
            // Audio data (conversion little endian -> big endian)
            short[] shorts = new short[actualWavAudio.length / 2];
            ByteBuffer.wrap(actualWavAudio).order(ByteOrder.LITTLE_ENDIAN).asShortBuffer().get(shorts);
            ByteBuffer bytes = ByteBuffer.allocate(shorts.length * 2);
            for (short s : shorts) {
                bytes.putShort(s);
            }
            output.write(bytes.array());
        } finally {
            if (output != null) {
                output.close();
                //rawFile.delete();
            }
        }
    }

    public static void writeInt(final DataOutputStream output, final int value) throws IOException {
        output.write(value >> 0);
        output.write(value >> 8);
        output.write(value >> 16);
        output.write(value >> 24);
    }

    public static void writeShort(final DataOutputStream output, final short value) throws IOException {
        output.write(value >> 0);
        output.write(value >> 8);
    }

    public static void writeString(final DataOutputStream output, final String value) throws IOException {
        for (int i = 0; i < value.length(); i++) {
            output.write(value.charAt(i));
        }
    }

    public static String concatenateFiles(String rootPath, String file1Name, String file2Name, String fileConcatenateName) {
        try {

            File lastConcatenateFile = new File(rootPath + "/" + "concatenate.raw");
            if (lastConcatenateFile.exists()) {
                lastConcatenateFile.delete();
            }

            File file1Raw = new File(rootPath + "/" + file1Name + ".raw");
            File file2Raw = new File(rootPath + "/" + file2Name + ".raw");
            file1Raw.createNewFile();
            file2Raw.createNewFile();
            final FileInputStream input1Stream = new FileInputStream(rootPath + "/" + file1Name + ".raw");
            final FileInputStream input2Stream = new FileInputStream(rootPath + "/" + file2Name + ".raw");

            File file1Wav = new File(rootPath + "/" + file1Name + ".wav");
            File file2Wav = new File(rootPath + "/" + file2Name + ".wav");

            if (file1Wav.exists() && file2Wav.exists()) {
                waveToRaw(file1Wav, file1Raw);
                waveToRaw(file2Wav, file2Raw);
            }
            else {
                Log.e(TAG, "Error finding file1wav or file2wav");
                return "";
            }

            final FileOutputStream outputStream = new FileOutputStream(rootPath + "/" + "concatenate" + ".raw", true);

            final FileChannel in1Channel = input1Stream.getChannel();
            final FileChannel in2Channel = input2Stream.getChannel();
            final FileChannel outChannel = outputStream.getChannel();

            //in1Channel.transferTo(0, in1Channel.size(), outChannel);
            in2Channel.transferTo(0, in2Channel.size(), outChannel);

            in1Channel.close();
            in2Channel.close();
            outChannel.close();

            input1Stream.close();
            input2Stream.close();
            outputStream.close();


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            rawToWave(new File(rootPath + "/" + "concatenate" + ".raw"),
                    new File(rootPath + "/" + fileConcatenateName + ".wav"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return rootPath + "/" + fileConcatenateName + ".wav";
    }

}
