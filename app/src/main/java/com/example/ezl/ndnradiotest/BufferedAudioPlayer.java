package com.example.ezl.ndnradiotest;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.ToneGenerator;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;

import com.example.ezl.ndnradiotest.PrioritizedByteArray;

public class BufferedAudioPlayer {

    private final String TAG = "BufferedAudioPlayer";

    public final static String
            ERROR_MESSAGE_PLAYER_APPROVE_START = "ERROR_MESSAGE_PLAYER_APPROVE_START",
            ERROR_MESSAGE_PLAYER_SIGNAL_END = "ERROR_MESSAGE_PLAYER_SIGNAL_END";

    public final static String
            EXTRA_FILE_NAME = "EXTRA_FILE_NAME";

    private BlockingQueue<byte[]> m_bufferedReceivedAudioClips;
    boolean m_currently_playing_buffered_audio = false;
    private MediaPlayer m_player = null;
    private String m_filePath = "";
    boolean m_is_playing = false;
    byte[] lastBytesPlayed = null;
    //private NDNRadioService m_radio_service;

    private Timer m_pause_timer;
    final int MAX_PAUSE_TIME = 60000;

    boolean m_currently_paused = false;

    BufferedAudioPlayer(String filePath) {
        m_bufferedReceivedAudioClips = new LinkedBlockingQueue<>();
        m_filePath = filePath;
        //m_radio_service = radioService;

        playBufferedAudioThread.start();
    }

    public void playNextAvailableAudioClip() {

        Log.d(TAG, "PEEK WAS CALLED ON BUFFERED AUDIO CLIPS IN playNextAvailableAudioClip");
        byte[] audioBytes = m_bufferedReceivedAudioClips.peek();

        if (audioBytes == null && m_bufferedReceivedAudioClips.size() == 0) {
            m_currently_playing_buffered_audio = false;
            Log.d(TAG, "Finished playing all buffered audio.");
            return;
        } else if (audioBytes == null) {
            Log.d(TAG, "Polling audio clips returned null timetsamped byte array but its size isnt zero, " +
                    "something is fishy...");
            return;
        }

        if (audioBytes != null) {
            playBytesAsAudio(audioBytes);

            File lastReceivedAudioFile = new File(m_filePath + "/" + "last.wav");
            try {
                lastReceivedAudioFile.createNewFile();
                BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(lastReceivedAudioFile));
                bos.write(audioBytes);
                bos.flush();
                bos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        } else {
            m_currently_playing_buffered_audio = false;
            Log.d(TAG, "Finished playing all buffered audio.");
        }
    }

    public void addAudioToAudioClipsBuffer(byte[] data) {
        Log.d(TAG, "Adding a byte array of length " + data.length + " to audio clips buffer");
        m_bufferedReceivedAudioClips.add(data);

    }

    private void playBytesAsAudio(final byte[] data) {

        if (Arrays.equals(data, lastBytesPlayed)) {
            Log.d(TAG, "Buffered audio player found a duplicate byte array being played, not playing it.");
            m_bufferedReceivedAudioClips.poll();
            m_is_playing = false;
            playNextAvailableAudioClip();
            return;
        }

        Log.d(TAG, "play bytes as audio: Length of audio bytes:" + data.length);

        final String tempFileName = UUID.randomUUID().toString();

        FileOutputStream out = null;
        try {
            out = new FileOutputStream(m_filePath + "/" + tempFileName);
        } catch (FileNotFoundException e) {
            Log.d(TAG, "Got a file not found exception.");
            e.printStackTrace();
        }
        try {
            out.write(data);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "Finished writing the received data to a file");

        m_player = new MediaPlayer();
        try {
            m_player.setDataSource(m_filePath + "/" + tempFileName);
            m_player.prepare();
            m_player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                @Override
                public void onCompletion(MediaPlayer mp) {
                    Log.d(TAG, "Finished playing an audio clip of length " + data.length);
                    m_bufferedReceivedAudioClips.poll();
                    m_is_playing = false;
                    m_player.release();
                    File tempFile = new File(m_filePath + "/" + tempFileName);
                    tempFile.delete();
                    lastBytesPlayed = Arrays.copyOf(data, data.length);
                    playNextAvailableAudioClip();
                }

            });
            m_player.start();

            m_is_playing = true;

        } catch (IOException e) {
            Log.d(TAG, "POLL WAS CALLED ON BUFFERED AUDIO CLIPS in playBytesAsAudio, in the catch failure");
            m_bufferedReceivedAudioClips.poll();
            m_currently_playing_buffered_audio = false;
            File tempFile = new File(m_filePath + "/" + tempFileName);
            tempFile.delete();
            Log.e(TAG, "prepare() failed");
            m_is_playing = false;
        }

    }

    Thread playBufferedAudioThread = new Thread(new Runnable() {

        public void run() {
            while (true) {
                int currentSize = m_bufferedReceivedAudioClips.size();
                if (currentSize != 0 && !m_currently_paused) {

                    Log.d(TAG, "buffered audio thread player noticed we received new audio");

                    if (!m_currently_playing_buffered_audio) {
                        m_currently_playing_buffered_audio = true;
                        Log.d(TAG, "Making initial call to play next available audio clip");
                        playNextAvailableAudioClip();
                    }
                }
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

    });

    private void startWaitTimer() {
        if (m_pause_timer != null) {
            m_pause_timer.cancel();
            m_pause_timer = null;
            m_pause_timer = new Timer();
        } else {
            m_pause_timer = new Timer();
        }
        m_pause_timer.schedule(new TimerTask() {
            @Override
            public void run() {
                Log.d(TAG, "Pause timer ran out, so we are resuming playing");
                resume();
            }

            @Override
            public boolean cancel() {

                Log.d(TAG, "pause timer got cancelled");

                return super.cancel();
            }
        }, MAX_PAUSE_TIME);
    }

    private void cancelWaitTimer() {
        if (m_pause_timer != null) {
            m_pause_timer.cancel();
            m_pause_timer = null;
        }
    }

    void resume() {

        cancelWaitTimer();

        m_currently_paused = false;

    }

    public void interruptPlayingToPlayThisFile(String fileRequestingToBePlayedName) {

        Log.d(TAG, "Prematurely stopped playing an audio clip.");

        m_currently_paused = true;

        if (m_player != null) {
            m_player.release();
            m_player = null;
        }

        m_is_playing = false;
        m_currently_playing_buffered_audio = false;

        Intent intent = new Intent(ERROR_MESSAGE_PLAYER_APPROVE_START);

        intent.putExtra(EXTRA_FILE_NAME, fileRequestingToBePlayedName);

        LocalBroadcastManager.getInstance(MainActivity.getInstance()).sendBroadcast(intent);

        cancelWaitTimer();
        startWaitTimer();
    }

    public void pause() {
        Log.d(TAG, "Prematurely stopped playing for a pause.");

        m_currently_paused = true;

        if (m_player != null) {
            m_player.release();
            m_player = null;
        }

        m_is_playing = false;
        m_currently_playing_buffered_audio = false;

        cancelWaitTimer();
        startWaitTimer();
    }

    public boolean isPlaying() {
        return m_is_playing;
    }

    public boolean isPlayingBufferedAudio() {
        return m_currently_playing_buffered_audio;
    }

    public boolean isPaused() {
        return m_currently_paused;
    }

    public static IntentFilter getIntentFilter() {
        IntentFilter filter = new IntentFilter();

        filter.addAction(ERROR_MESSAGE_PLAYER_APPROVE_START);
        filter.addAction(ERROR_MESSAGE_PLAYER_SIGNAL_END);

        return filter;
    }
}
