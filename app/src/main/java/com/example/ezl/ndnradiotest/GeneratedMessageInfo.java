package com.example.ezl.ndnradiotest;

import android.support.annotation.NonNull;

public class GeneratedMessageInfo implements Comparable<GeneratedMessageInfo> {

    GeneratedMessageInfo(String userId, String userName, long seqNum, String errorCode)  {
        this.userId = userId;
        this.userName = userName;
        this.seqNum = seqNum;
        this.errorCode = errorCode;
    }

    String userId;
    String userName;
    long seqNum;
    // this is just for message failure generation infos; whether the failure message is for
    // failed retrieval or failed verification
    String errorCode;

    @Override
    public int compareTo(@NonNull GeneratedMessageInfo other) {
        if (userId.equals(other.userId) && userName.equals(other.userName) && seqNum == other.seqNum)
            return 0;
        return -1;
    }
}
