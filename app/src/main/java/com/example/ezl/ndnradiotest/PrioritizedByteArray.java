package com.example.ezl.ndnradiotest;

import android.support.annotation.NonNull;

public class PrioritizedByteArray implements Comparable<PrioritizedByteArray> {

    PrioritizedByteArray(long priority, byte[] array) {
        this.priority = priority;
        this.array = array;
    }

    public long priority;
    public byte[] array;

    @Override
    public int compareTo(@NonNull PrioritizedByteArray other) {
        if (this.priority > other.priority)
            return 1;
        else if (this.priority < other.priority)
            return -1;

        return 0;
    }
}
