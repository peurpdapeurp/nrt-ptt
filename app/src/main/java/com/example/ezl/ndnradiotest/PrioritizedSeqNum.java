package com.example.ezl.ndnradiotest;

import android.support.annotation.NonNull;

public class PrioritizedSeqNum implements Comparable<PrioritizedSeqNum> {

    PrioritizedSeqNum(long seqNum) {
        this.seqNum = seqNum;
    }

    @Override
    public int compareTo(@NonNull PrioritizedSeqNum other) {
        if (seqNum < other.seqNum)
            return -1;
        else if (seqNum > other.seqNum)
            return 1;

        return 0;
    }

    public long seqNum;
}
