package com.example.ezl.ndnradiotest;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class LongByteArrayConversionHelpers {

    public static final int LONG_NUM_BYTES = Long.SIZE / Byte.SIZE;

    public static byte[] longToBytes(long l) {
        byte[] result = new byte[LONG_NUM_BYTES];
        for (int i = LONG_NUM_BYTES - 1; i >= 0; i--) {
            result[i] = (byte) (l & 0xFF);
            l >>= LONG_NUM_BYTES;
        }
        return result;
    }

    public static long bytesToLong(byte[] b) {
        long result = 0;
        for (int i = 0; i < LONG_NUM_BYTES; i++) {
            result <<= LONG_NUM_BYTES;
            result |= (b[i] & 0xFF);
        }
        return result;
    }

    public static String longToTime(long timestamp) {

        Date date = new Date(timestamp);
        DateFormat formatter = new SimpleDateFormat("HH:mm:ss.SSS");
        formatter.setTimeZone(TimeZone.getTimeZone("EST"));
        String dateFormatted = formatter.format(date);

        return dateFormatted;
    }

}
