package com.example.ezl.ndnradiotest;

import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import static com.example.ezl.ndnradiotest.FragmentationHelpers.createFragments;
import static com.example.ezl.ndnradiotest.FragmentationHelpers.getNumberOfFragments;
import static com.example.ezl.ndnradiotest.WavToAndFromRawHelpers.rawToWave;
import static org.apache.commons.io.FileUtils.readFileToByteArray;

public class LocalAudioManager {

    private final String TAG = "AudioManager";

    public static String
            ACTION_STARTED_PLAYING = "ACTION_STARTED_PLAYING",
            ACTION_STOPPED_PLAYING = "ACTION_STOPPED_PLAYING",
            ACTION_STARTED_RECORDING = "ACTION_STARTED_RECORDING",
            ACTION_STOPPED_RECORDING = "ACTION_STOPPED_RECORDING";

    private MediaPlayer m_player = null;
    private MediaRecorder m_recorder = null;
    private boolean m_is_recording;
    private boolean m_is_playing;
    private String m_rootPath;

    LocalAudioManager(String file_name) {
        m_is_recording = false;
        m_is_playing = false;
        m_rootPath = file_name;
    }

    public boolean isRecording() {
        return m_is_recording;
    }

    public boolean isPlaying() {
        return m_is_playing;
    }

    public void startPlaying(String fileName) {

        if (isRecording()) {
            Log.d(TAG, "Not playing as we are currently recording.");
            return;
        }

        if (isPlaying()) {
            Log.d(TAG, "Not playing as we are already playing.");
            return;
        }

        Log.d(TAG, "Trying to play clip with file path: " + fileName);

        m_player = new MediaPlayer();
        try {
            m_player.setDataSource(fileName);
            m_player.prepare();
            m_player.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mediaPlayer) {
                    m_player.start();
                }
            });
            m_player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer) {
                    stopPlaying();
                }
            });
        } catch (IOException e) {
            Log.e(TAG, "prepare() failed");
            stopPlaying();
            return;
        }

        m_is_playing = true;

        Intent intent = new Intent(ACTION_STARTED_PLAYING);
        LocalBroadcastManager.getInstance(MainActivity.getInstance()).sendBroadcast(intent);
    }

    public void stopPlaying() {
        if (m_player != null) {
            m_player.release();
            m_player = null;
        }

        m_is_playing = false;

        Intent intent = new Intent(ACTION_STOPPED_PLAYING);
        LocalBroadcastManager.getInstance(MainActivity.getInstance()).sendBroadcast(intent);
    }

    public void startRecording() {

        if (isRecording()) {
            Log.d(TAG, "Not recording as we are already recording.");
            return;
        }

        if (isPlaying()) {
            Log.d(TAG, "Not recording as we are currently playing.");
            return;
        }

        m_recorder = new MediaRecorder();
        m_recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        m_recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        m_recorder.setOutputFile(m_rootPath + "/" + "final.wav");
        m_recorder.setAudioSamplingRate(22050);
        m_recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

        try {
            m_recorder.prepare();
        } catch (IOException e) {
            Log.e(TAG, "prepare() failed");
        }

        m_recorder.start();

        m_is_recording = true;

        Intent intent = new Intent(ACTION_STARTED_RECORDING);
        LocalBroadcastManager.getInstance(MainActivity.getInstance()).sendBroadcast(intent);
    }

    public void stopRecording() {
        if (m_recorder != null) {
            try {
                m_recorder.stop();
            }
            catch (Exception e) {
                Log.w(TAG, "Exception for recorder stop: " + e.getMessage());
            }
            m_recorder.release();
            m_recorder = null;
        }

        m_is_recording = false;

        Intent intent = new Intent(ACTION_STOPPED_RECORDING);
        LocalBroadcastManager.getInstance(MainActivity.getInstance()).sendBroadcast(intent);
    }

    public void close() {
        if (m_recorder != null) {
            m_recorder.release();
            m_recorder = null;
        }

        if (m_player != null) {
            m_player.release();
            m_player = null;
        }
    }

    public static IntentFilter getIntentFilter() {
        IntentFilter filter = new IntentFilter();

        filter.addAction(ACTION_STARTED_PLAYING);
        filter.addAction(ACTION_STOPPED_PLAYING);
        filter.addAction(ACTION_STARTED_RECORDING);
        filter.addAction(ACTION_STOPPED_RECORDING);

        return filter;
    }
}


/*
public class LocalAudioManager {

    private final String TAG = "AudioManager";

    public static String
            ACTION_STARTED_PLAYING = "ACTION_STARTED_PLAYING",
            ACTION_STOPPED_PLAYING = "ACTION_STOPPED_PLAYING",
            ACTION_STARTED_RECORDING = "ACTION_STARTED_RECORDING",
            ACTION_STOPPED_RECORDING = "ACTION_STOPPED_RECORDING";

    private MediaPlayer m_player = null;
    private boolean m_is_recording;
    private boolean m_is_playing;
    public static int SAMPLE_RATE;

    private AudioRecord mRecorder;
    private File mRecording;
    private short[] mBuffer;

    private String m_rootPath;

    LocalAudioManager(String rootPath) {
        m_is_recording = false;
        m_is_playing = false;
        m_rootPath = rootPath;
    }

    // Initializing AudioRecording MIC
    private void initRecorder() {
        SAMPLE_RATE = 8000;
        Log.d(TAG, "SAMPLE_RATE: " + Integer.toString(SAMPLE_RATE));
        int bufferSize = AudioRecord.getMinBufferSize(SAMPLE_RATE, AudioFormat.CHANNEL_IN_MONO,
                AudioFormat.ENCODING_PCM_16BIT);
        mBuffer = new short[bufferSize];
        mRecorder = new AudioRecord(MediaRecorder.AudioSource.MIC, SAMPLE_RATE, AudioFormat.CHANNEL_IN_MONO,
                AudioFormat.ENCODING_PCM_16BIT, bufferSize);
    }

    public boolean isRecording() {
        return m_is_recording;
    }

    public boolean isPlaying() {
        return m_is_playing;
    }

    public void startPlaying(String fileName) {

        if (isRecording()) {
            Log.d(TAG, "Not playing as we are currently recording.");
            return;
        }

        if (isPlaying()) {
            Log.d(TAG, "Not playing as we are already playing.");
            return;
        }

        Log.d(TAG, "Trying to play clip with file path: " + fileName);

        m_player = new MediaPlayer();
        try {
            m_player.setDataSource(fileName);
            m_player.prepare();
            m_player.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mediaPlayer) {
                    m_player.start();
                }
            });
            m_player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer) {
                    stopPlaying();
                }
            });
        } catch (IOException e) {
            Log.e(TAG, "prepare() failed");
        }

        m_is_playing = true;

        Intent intent = new Intent(ACTION_STARTED_PLAYING);
        LocalBroadcastManager.getInstance(MainActivity.getInstance()).sendBroadcast(intent);
    }

    public void stopPlaying() {
        if (m_player != null) {
            m_player.release();
            m_player = null;
        }

        m_is_playing = false;

        Intent intent = new Intent(ACTION_STOPPED_PLAYING);
        LocalBroadcastManager.getInstance(MainActivity.getInstance()).sendBroadcast(intent);
    }

    public void startRecording() {

        if (isRecording()) {
            Log.d(TAG, "Not recording as we are already recording.");
            return;
        }

        if (isPlaying()) {
            Log.d(TAG, "Not recording as we are currently playing.");
            return;
        }

        initRecorder();

        mRecorder.startRecording();
        mRecording = new File(m_rootPath + "/" + "recording.raw");
        startBufferedWrite(mRecording);

        m_is_recording = true;

        Intent intent = new Intent(ACTION_STARTED_RECORDING);
        LocalBroadcastManager.getInstance(MainActivity.getInstance()).sendBroadcast(intent);
    }

    public void stopRecording() {
        try {
            m_is_recording = false;
            mRecorder.stop();
            //File waveFile = getFile("wav");
            File waveFile = new File(m_rootPath + "/" + "recording.wav");
            rawToWave(mRecording, waveFile);
        } catch (Exception e) {
            Log.e("Error saving file : ", e.getMessage());
        }

        Intent intent = new Intent(ACTION_STOPPED_RECORDING);
        LocalBroadcastManager.getInstance(MainActivity.getInstance()).sendBroadcast(intent);
    }

    public void close() {
        if (mRecorder != null) {
            mRecorder.release();
            mRecorder = null;
        }

        if (m_player != null) {
            m_player.release();
            m_player = null;
        }
    }

    public static IntentFilter getIntentFilter() {
        IntentFilter filter = new IntentFilter();

        filter.addAction(ACTION_STARTED_PLAYING);
        filter.addAction(ACTION_STOPPED_PLAYING);
        filter.addAction(ACTION_STARTED_RECORDING);
        filter.addAction(ACTION_STOPPED_RECORDING);

        return filter;
    }

    // Writing RAW file
    private void startBufferedWrite(final File file) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                DataOutputStream output = null;
                try {
                    output = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(file)));
                    while (m_is_recording) {
                        double sum = 0;
                        int readSize = mRecorder.read(mBuffer, 0, mBuffer.length);
                        for (int i = 0; i < readSize; i++) {
                            output.writeShort(mBuffer[i]);
                            sum += mBuffer[i] * mBuffer[i];
                        }
                        if (readSize > 0) {
                            final double amplitude = sum / readSize;
                        }
                    }
                } catch (IOException e) {
                    Log.e("Error writing file : ", e.getMessage());
                } finally {

                    if (output != null) {
                        try {
                            output.flush();
                        } catch (IOException e) {
                            Log.e("Error writing file : ", e.getMessage());
                        } finally {
                            try {
                                output.close();
                            } catch (IOException e) {
                                Log.e("Error writing file : ", e.getMessage());
                            }
                        }
                    }
                }
            }
        }).start();
    }
}
*/
