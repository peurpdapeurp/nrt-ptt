package com.example.ezl.ndnradiotest;

public class UserIdAndName {

    UserIdAndName(String userId, String name) {
        this.userId = userId;
        this.name = name;
    }

    String userId;
    String name;
}
